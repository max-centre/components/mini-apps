!
! License-Identifier: GPL
!
! Copyright (C) 2013 The Yambo Team
!
! Authors (see AUTHORS file for details): AM
!
#include<y_memory.h>
!
subroutine PARALLEL_add_Q_to_K_list(ZONE,IND_in,IND_in_ID,IND_k_out,IND_k_out_ID,&
&                                   IND_q,COMM,q_range,k,q)
 !
 use parallel_m,    ONLY:PP_indexes,yMPI_comm
 use R_lattice,     ONLY:qindx_X,nkbz,nkibz,bz_samp,nqbz,qindx_S,qindx_B
 !
 implicit none
 !
 character(*)           ::ZONE
 type(PP_indexes)       ::IND_in,IND_k_out,IND_q
 integer                ::IND_in_ID,IND_k_out_ID,q_range(2)
 type(yMPI_comm)         ::COMM
 type(bz_samp)          ::k,q
 !
 integer :: iq,ikp,ikbz,ik,ikpbz,iqp,iqbz
 integer :: id,istep,idx_kp(3),idx_k(3),max_step,ikbzp
 !
 IND_k_out_ID=IND_in_ID
 !
 YAMBO_ALLOC(IND_k_out%n_of_elements,(COMM%n_CPU))
 YAMBO_ALLOC(IND_k_out%element_1D,(nkibz))
 !
 IND_k_out%n_of_elements(IND_k_out_ID+1)=0
 IND_k_out%element_1D(:)=.FALSE.
 !
 if (ZONE=="k_bz_q_ibz") then
   !
   do ikbz=1,nkbz
     !
     if (.not.IND_in%element_1D(ikbz)) cycle
     !
     ik  =k%sstar(ikbz,1)
     !
     if (.not.IND_k_out%element_1D(ik)) then
       IND_k_out%element_1D(ik)=.TRUE.
       IND_k_out%n_of_elements(IND_k_out_ID+1)=IND_k_out%n_of_elements(IND_k_out_ID+1)+1
     endif
     !
     do iq=q_range(1),q_range(2)
       !
       if (.not.IND_q%element_1D(iq)) cycle
       !
       ikpbz=qindx_X(iq,ikbz,1)
       ikp  =k%sstar(ikpbz,1)
       !
       if (.not.IND_k_out%element_1D(ikp)) then
         IND_k_out%element_1D(ikp)=.TRUE.
         IND_k_out%n_of_elements(IND_k_out_ID+1)=IND_k_out%n_of_elements(IND_k_out_ID+1)+1
       endif
       !
     enddo
     !
   enddo
   !
 else if (ZONE=="k_bz_q_bz".or.ZONE=="k_bz_q_bz_p_bz") then
   !
   do ikbz=1,nkbz
     !
     if (.not.IND_in%element_1D(ikbz)) cycle
     !
     ik=k%sstar(ikbz,1)
     if (ZONE=="k_bz_q_bz_p_bz") ik=ikbz
     !
     if (.not.IND_k_out%element_1D(ik)) then
       IND_k_out%element_1D(ik)=.TRUE.
       IND_k_out%n_of_elements(IND_k_out_ID+1)=IND_k_out%n_of_elements(IND_k_out_ID+1)+1
     endif 
     !
     do iq=1,nqbz
       !
       if (.not.IND_q%element_1D(iq)) cycle
       !
       ! This is strange qindx_B is allocated nK,nK, why ranging over q ?
       !
       ikpbz=qindx_B(ikbz,iq,1)
       ikp  =k%sstar(ikpbz,1)
       if (ZONE=="k_bz_q_bz_p_bz") ikp=ikpbz
       !
       if (.not.IND_k_out%element_1D(ikp)) then
         IND_k_out%element_1D(ikp)=.TRUE.
         IND_k_out%n_of_elements(IND_k_out_ID+1)=IND_k_out%n_of_elements(IND_k_out_ID+1)+1
       endif
       !
     enddo
     !
   enddo
   !
 endif
 !
end subroutine PARALLEL_add_Q_to_K_list
