!
! License-Identifier: GPL
!
! Copyright (C) 2017 The Yambo Team
!
! Authors (see AUTHORS file for details): AM DS
!
subroutine PARALLEL_global_dimensions(E,Xk,q,ENVIRONMENT)
 !
 use parallel_m,    ONLY:PAR_Q_bz_range,PAR_Q_ibz_range,PAR_QP_range,PAR_n_v_bands,PAR_n_c_bands,&
&                        PAR_EH_range,PAR_Dip_ib,PAR_Dip_ib_lim,PAR_X_ib,PAR_X_iq,PAR_n_G_vectors,&
&                        PAR_n_freqs,PAR_K_range,PAR_n_bands
 use X_m,           ONLY:X_ng
 use electrons,     ONLY:levels
 use R_lattice,     ONLY:bz_samp,nqibz,nqbz
 !
 implicit none
 !
 type(levels)         :: E
 type(bz_samp)        :: Xk,q
 character(*)         :: ENVIRONMENT
 !
 ! Work Space
 !
 integer              :: i_k
 logical :: computing_Fock
 !
 computing_Fock= .false.
 !
 ! K-pts
 !-------
 PAR_K_range=Xk%nibz
 if (ENVIRONMENT=="BZ_Indexes"               ) PAR_K_range=Xk%nibz
 if (ENVIRONMENT=="DIPOLES"                  ) PAR_K_range=Xk%nibz
 if (index(ENVIRONMENT, "Response_G_space")>0) PAR_K_range=Xk%nbz
 !
 ! COND bands
 !------------
 if (ENVIRONMENT=="DIPOLES"                  ) PAR_n_c_bands= (/PAR_Dip_ib_lim(2),PAR_Dip_ib(2)/)
 if (index(ENVIRONMENT, "Response_G_space")>0) PAR_n_c_bands= (/minval(E%nbf)+1,PAR_X_ib(2)/)
 if (ENVIRONMENT=="Response_G_space"         ) PAR_n_c_bands= (/minval(E%nbf)+1,PAR_X_ib(2)/)
 !
 ! VAL bands
 !-----------
 if (ENVIRONMENT=="DIPOLES")                          PAR_n_v_bands= (/PAR_Dip_ib(1),PAR_Dip_ib_lim(1)/)
 if (index(ENVIRONMENT, "Response_G_space")>0       ) PAR_n_v_bands= (/PAR_X_ib(1),maxval(E%nbm)/)
 !
 ! # G vectors
 !-------------
 if (index(ENVIRONMENT, "Response_G_space")>0) PAR_n_G_vectors= X_ng
 !
 ! Q-pts
 !-------
 PAR_Q_ibz_range=(/1,nqibz/)
 PAR_Q_bz_range=(/1,nqbz/)
 if (ENVIRONMENT=="Response_G_space"  )        PAR_Q_ibz_range=(/PAR_X_iq(1),PAR_X_iq(2)/)
 !
end subroutine PARALLEL_global_dimensions
