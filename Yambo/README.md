# Yambo MaX Mini-app
This is the Yambo Mini-app from the MaX Centre of Excellence.

## Origin
The Yambo mini-app originates from the [`yambo-code`](https://github.com/yambo-code/yambo) \
It is extracted from the tag [5.3-beta](https://github.com/yambo-code/yambo/releases/tag/5.3-beta) (a pre-release). 
Release 5.3.0 expected by the end of November 2024

## Short description
The Yambo mini-app computes the independent particle polarizability in reciprocal space: $`\chi^0_{G,G'}(q,w)`$.
This constitutes one of the main kernels of the yambo code since it is used for building the screened dielectric function. 

After loading in memory the Khon-Sham wave-functions (WFs), $`\chi^0_{G,G'}(q,w)`$ is computed via a large loop done inside the subroutine \
[`X_irredux.f90`](https://gitlab.com/max-centre/components/mini-apps/-/blob/main/Yambo/src/X_irredux.f90) .
See equations here: [Response equations](https://wiki.yambo-code.eu/wiki/index.php?title=File:Yambo-Cheatsheet-5.0_P7.png) .

To compute the oscillators $`\rho_{nmk}(q,G)`$ the subroutine [`X_irredux_resiudals.f90`](https://gitlab.com/max-centre/components/mini-apps/-/blob/main/Yambo/src/X_irredux_residuals.f90) is called. 
See equations here: [Residuals equations](https://wiki.yambo-code.eu/wiki/index.php?title=File:Yambo-Cheatsheet-5.0_P6.png) .

At this stage the WFs are used and the oscillators are obtained by doing: \
(i) a product of WFs in real-space \
(ii) a fourier transform to G space

In standard Yambo runs the WFs are loaded from a previously computed database. In the mini-app this procedure is avoided by generating random wave-functions. As a consequence the output of the simulations cannot be checked.

## Build instructions
Open the file `make.inc` and fill the relevant variables.
Exit and run \
`$ make all `

List of dependencies: fftw, blas, lapack

## Run instructions
In order to run `miniapp_X.x` one requires an input datafile, containing the main dimensions of 
the problem.
Examples taken from realistic systems are provided within the files \
`tests/dataset_0.dat`\
`tests/dataset_1.dat` 

A typical run is triggered by issuing the command: \
`$./bin/miniapp_X.x -f tests/dataset_1.dat`

Note that some critical dimensions can be tuned by command-line arguments, according to: \
`miniapp_X.x  -f <filename> [ -nb <nbnd> ] [-ng <X_ng>] [-nq <X_nq>] [-nw <nfreq>]`

In the above:
`<nbnd>`      is the number of bands included in the sum-over-states \
`<X_ng>`      is the number of plane-waves used to represent the X matrix \
`<X_nq>`      is the number of q-points where X is computed \
`<X_nfreq>`   is the number of frequencies for which X is computed.

By tuning these dimensions, the time-to-solution of the runs can be significantly altered.

In output run-times are provided via internal clocks coded in the mini-app.

## Test cases description
* `tests/dataset_0.dat` takes <1 s in serial. It is there mostly to check that the mini-app is workin.
* `tests/dataset_1.dat` is an example and takes aboud 20-60 seconds per q-point. The number of q-points and other parameters can be changed in input to make the test slower/faster. Relevant parameters belo  
```
n_sp_pol:   1
n_spinor:   1
nsym:      12
nqibz:     10
nqbz:      64
nkibz:     10
nkbz:      64
nbnd:     100
nbnd_occ:   4
ng_vec:  23863
wf_ncx:   1739
wf_ng:    2729
X_ng:      500
X_nfreq:     2
```


## Test status
1. Serial [OK]
2. Multi-threads (`OpenMP`) [OK]
3. Multi cores (`MPI`) [TODO]
4. GPU porting (`CudaFortran`) [TODO]
5. GPU porting (`OpenACC`) [TODO]
