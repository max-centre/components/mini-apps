
subroutine main_vars_init(Xen,Xk,Xw,filedat)
  !
  use pars,         ONLY:SP,pi
  use stderr,       ONLY:l_write_output,debug_level
  use electrons,    ONLY:levels,nel,n_sp_pol,n_spinor,n_spin,spin_occ
  use wave_func,    ONLY:wf_ng,wf_ncx
  use R_lattice,    ONLY:nqibz,nqbz,nkibz,nkbz,nXkbz,ng_vec,b,g_vec,k_pt,q_pt,rl_sop,bz_samp,&
  &                      qindx_X,ng_closed
  use parallel_m,   ONLY:l_yminiapp_par,PAR_COM_X_WORLD,PAR_COM_RL_INDEX
  use frequency,    ONLY:w_samp
  use D_lattice,    ONLY:nsym,a,alat,DL_vol,dl_sop,alat,symmetry_group_table
  use vec_operate,  ONLY:define_b_and_DL_vol,c2a
  use zeros,        ONLY:k_iku_zero,k_rlu_zero,G_iku_zero,define_zeros,G_mod_zero
  use input_m,      ONLY:X_ng,X_ng_cmdline,nbnd,nbnd_cmdline,&
                         X_nq,X_nq_cmdline,X_nfreq,X_nfreq_cmdline,&
                         nbnd_occ,ecutwfc,X_nfreq
  !
  implicit none
  !
  type(levels)  :: Xen
  type(bz_samp) :: Xk
  type(w_samp)  :: Xw
  character(*)  :: filedat
  !
  integer  :: iun, ind, ierr, i1, i2, i3, i
  integer  :: ib, ik, is, iq
  character(256) :: str, line
  real(SP) :: k_plus_g(3), v_cc(3)
  real(SP) :: a_cc(3,3), b_cc(3,3), rtmp(3,3)
  real(SP), allocatable :: k_pt_loc(:,:),q_pt_loc(:,:)
  !
  iun=1001
  l_yminiapp_par=.false.
  PAR_COM_X_WORLD%n_CPU=1
  PAR_COM_X_WORLD%CPU_id=0
  PAR_COM_RL_INDEX%n_CPU=1
  !
  ! Setup main dimensions
  ! Data are read from filedat
  !
  open(iun,file=filedat,iostat=ierr)
  if (ierr/=0) call error("opening file "//trim(filedat))

  call io_get_line_data(iun,"ecutwfc",line,ind,ierr)
  if (ierr/=0) call error("reading ecutwfc")
  str=trim(line(ind:))
  read(str,*) ecutwfc
  !
  call io_get_line_data(iun,"alat",line,ind,ierr)
  if (ierr/=0) call error("reading alat")
  str=trim(line(ind:))
  read(str,*) alat
  call io_get_line_data(iun,"a_vec",line,ind,ierr)
  if (ierr/=0) call error("reading a_vec")
  read(iun,*) rtmp(:,:)
  a = transpose(rtmp)
  !
  call io_get_line_data(iun,"n_sp_pol",line,ind,ierr)
  if (ierr/=0) call error("reading n_sp_pol")
  str=trim(line(ind:))
  read(str,*) n_sp_pol
  call io_get_line_data(iun,"n_spinor",line,ind,ierr)
  if (ierr/=0) call error("reading n_spinor")
  str=trim(line(ind:))
  read(str,*) n_spinor

  if (n_sp_pol==2 .or. n_spinor==2) then
    n_spin=2
    spin_occ=1._SP
  else
    n_spin=1
    spin_occ=2._SP
  endif

  call io_get_line_data(iun,"nsym",line,ind,ierr)
  if (ierr/=0) call error("reading nsym")
  str=trim(line(ind:))
  read(str,*) nsym
  !
  call io_get_line_data(iun,"nqibz",line,ind,ierr)
  if (ierr/=0) call error("reading nqibz")
  str=trim(line(ind:))
  read(str,*) nqibz
  call io_get_line_data(iun,"nqbz",line,ind,ierr)
  if (ierr/=0) call error("reading nqbz")
  str=trim(line(ind:))
  read(str,*) nqbz
  call io_get_line_data(iun,"nkibz",line,ind,ierr)
  if (ierr/=0) call error("reading nkibz")
  str=trim(line(ind:))
  read(str,*) nkibz
  call io_get_line_data(iun,"nkbz",line,ind,ierr)
  if (ierr/=0) call error("reading nkbz")
  str=trim(line(ind:))
  read(str,*) nkbz
  nXkbz=nkbz
  !
  call io_get_line_data(iun,"nbnd",line,ind,ierr)
  if (ierr/=0) call error("reading nbnd")
  str=trim(line(ind:))
  read(str,*) nbnd
  call io_get_line_data(iun,"nbnd_occ",line,ind,ierr)
  if (ierr/=0) call error("reading nbnd_occ")
  str=trim(line(ind:))
  read(str,*) nbnd_occ
  !
  call io_get_line_data(iun,"ng_vec",line,ind,ierr)
  if (ierr/=0) call error("reading ng_vec")
  str=trim(line(ind:))
  read(str,*) ng_vec
  call io_get_line_data(iun,"wf_ncx",line,ind,ierr)
  if (ierr/=0) call error("reading wf_ncx")
  str=trim(line(ind:))
  read(str,*) wf_ncx
  call io_get_line_data(iun,"wf_ng",line,ind,ierr)
  if (ierr/=0) call error("reading wf_ng")
  str=trim(line(ind:))
  read(str,*) wf_ng
  call io_get_line_data(iun,"X_ng",line,ind,ierr)
  if (ierr/=0) call error("reading X_ng")
  str=trim(line(ind:))
  read(str,*) X_ng
  call io_get_line_data(iun,"X_nfreq",line,ind,ierr)
  if (ierr/=0) call error("reading X_nfreq")
  str=trim(line(ind:))
  read(str,*) X_nfreq

  if (nbnd_cmdline /=0 ) nbnd=nbnd_cmdline
  if (X_ng_cmdline /=0 ) X_ng=X_ng_cmdline
  X_nq=nqibz
  if (X_nq_cmdline /=0 ) X_nq=X_nq_cmdline
  if (X_nfreq_cmdline /=0 ) X_nfreq=X_nfreq_cmdline

  if (l_write_output) then
    !
    ! reporting
    !
    write(*,"(a20,':',f12.6)") "ecutwfc", ecutwfc
    write(*,"(a20,':',3f12.6)") "alat", alat
    write(*,"(a20,':')") "a"
    write(*,"(21x,3f12.6)") transpose(a)
    write(*,*)
    !
    write(*,"(a20,':',i6)") "n_sp_pol", n_sp_pol
    write(*,"(a20,':',i6)") "n_spinor", n_spinor
    write(*,"(a20,':',i6)") "nsym", nsym
    write(*,*)
    !
    write(*,"(a20,':',i6)") "nqibz", nqibz
    write(*,"(a20,':',i6)") "nqbz", nqbz
    write(*,"(a20,':',i6)") "nkibz", nkibz
    write(*,"(a20,':',i6)") "nkbz", nkbz
    write(*,*)
    !
    write(*,"(a20,':',i6)") "nbnd", nbnd
    write(*,"(a20,':',i6)") "nbnd_occ", nbnd_occ
    write(*,*)
    !
    write(*,"(a20,':',i6)") "ng_vec", ng_vec
    write(*,"(a20,':',i6)") "wf_ncx", wf_ncx
    write(*,"(a20,':',i6)") "wf_ng", wf_ng
    write(*,"(a20,':',i6)") "X_ng", X_ng
    write(*,"(a20,':',i6)") "X_nq", X_nq
    write(*,"(a20,':',i6)") "X_nfreq", X_nfreq
    write(*,*)
    !
  endif
  !
  ! further init data
  !
  a_cc=a
  do i = 1, 3
     a_cc(i,:) = a_cc(i,:) * alat(:)
  enddo

  if (l_write_output) then
    write(*,"(a21)") "a iru:"
    write(*,"(21x,3f12.6)") transpose(a)
    write(*,"(a21)") "a cc:"
    write(*,"(21x,3f12.6)") transpose(a_cc)
  endif
  !
  ! generate reciprocal lattice
  call define_b_and_DL_vol(a_cc,b_cc,DL_vol)
  !
  if (l_write_output) write(*,"(a20,':',f12.6)") "DL volume", DL_vol
  !
  b = b_cc
  do i = 1, 3
     b(i,:) = b(i,:) * alat(:)/(2.0_SP*pi) 
  enddo
  if (l_write_output) then
    write(*,"(a21)") "b iku:"
    write(*,"(21x,3f12.6)") transpose(b)
    write(*,"(a21)") "b cc:"
    write(*,"(21x,3f12.6)") transpose(b_cc)
  endif
  !
  ! internally, use cc coords
  a = a_cc
  b = b_cc
  

  !
  !
  ! symmetries
  !
  allocate(dl_sop(3,3,nsym))
  call io_get_line_data(iun,"syms",line,ind,ierr)
  if (ierr/=0) call error("reading syms")
  read(iun,*) dl_sop(:,:,:)
  !
  allocate(rl_sop(3,3,nsym))
  !
  if (l_write_output) then
    write(*,*)
    write(*,"(a21)") "Symmetries:"
  endif
  do is=1,nsym
    forall (i2=1:3,i3=1:3) rl_sop(i2,i3,is)=dl_sop(i2,i3,is)*alat(i2)/alat(i3)
    if (l_write_output) write(*,"(21x,9f12.6)") dl_sop(:,:,is)
  enddo 
  !
  call symmetry_group_table()

  !
  ! K/Q-points section
  !
  allocate(k_pt_loc(3,nkibz))
  allocate(k_pt(nkibz,3))
  call io_get_line_data(iun,"kpoints",line,ind,ierr)
  if (ierr/=0) call error("reading kpoints")
  read(iun,*) k_pt_loc(:,:)
  !
  if (l_write_output) then
    write(*,*)
    write(*,"(a21)") "kpoints-ibz:"
  endif
  do ik=1,nkibz
    k_pt(ik,:)=k_pt_loc(:,ik)
    if (l_write_output) write(*,"(21x,3f12.6)") k_pt_loc(:,ik)
  enddo
  deallocate(k_pt_loc)
  !
  if (l_write_output) write(*,*)
  allocate(k_pt_loc(nkibz,3))
  do ik=1,nkibz
    call c2a(v_in=k_pt(ik,:),v_out=k_pt_loc(ik,:),mode="ki2a")
    if (l_write_output) write(*,"(21x,3f12.6)") k_pt_loc(ik,:)
  enddo
  deallocate(k_pt_loc)

  !
  ! shadow to Xk
  !
  Xk%nibz=nkibz
  Xk%nbz=nkbz
  allocate(Xk%pt(nkibz,3))
  Xk%pt=k_pt
  
  !
  ! Q-points IBZ
  !
  allocate(q_pt_loc(3,nqibz))
  allocate(q_pt(nqibz,3))
  call io_get_line_data(iun,"qpoints",line,ind,ierr)
  if (ierr/=0) call error("reading kpoints")
  read(iun,*) q_pt_loc(:,:)
  !
  if (l_write_output) then
    write(*,*)
    write(*,"(a21)") "qpoints-ibz:"
  endif
  do iq=1,nqibz
    q_pt(iq,:)=q_pt_loc(:,iq)
    if (l_write_output) write(*,"(21x,3f12.6)") q_pt_loc(:,iq)
  enddo
  deallocate(q_pt_loc)
  
  !
  ! tmp trivial qindx_X
  !
  allocate(qindx_X(nqibz,nXkbz,2))
  do iq=1,nqibz
    do ik=1,nXkbz
      qindx_X(iq,ik,1)=mod(ik+iq-1,nXkbz)+1
      qindx_X(iq,ik,2)=1
    enddo
  enddo
  !
  ! generate g vectors
  !
  ng_closed=ng_vec
  allocate(g_vec(ng_vec,3))
  call build_gvec(ecutwfc,ng_vec,g_vec)
  !
  call define_zeros(vector_=g_vec,zero_=G_iku_zero)
  call define_zeros(vector_=Xk%pt, zero_=k_iku_zero)
  call define_zeros(vector_=Xk%pt, zero_=k_rlu_zero,RLU=.TRUE.)
  G_mod_zero=1.0d-5
  !
  call G_shells_finder()
  if (l_write_output) write(*,"(a20,':',i6)") "ng_vec (closed)", ng_closed
  call PARSER_close_G(X_ng,'tRL')
  if (l_write_output) write(*,"(a20,':',i6)") "X_ng (closed)", X_ng

  !
  ! expand kpts from IBZ to BZ
  !
  call k_ibz2bz(Xk,'i',.true.)
  ! 
  if (l_write_output) then
    write(*,*)
    write(*,"(a21,2x,i5)") "kpoints-bz (iku):", Xk%nbz
    do ik=1,nkbz
      write(*,"(21x,3f12.6)") Xk%ptbz(ik,:)
    enddo
    write(*,*)
  endif
  !
  !allocate(k_pt_loc(nkbz,3))
  !write(*,"(a21,2x,i5)") "kpoints-bz (rlu):", Xk%nbz
  !do ik=1,nkbz
  !  call c2a(v_in=Xk%ptbz(ik,:),v_out=k_pt_loc(ik,:),mode="ki2a")
  !  write(*,"(21x,3f12.6)") k_pt_loc(ik,:)
  !enddo
  !deallocate(k_pt_loc)

  !
  ! setting band indexes and values
  !
  Xen%nb=nbnd
  Xen%nbm=nbnd_occ
  Xen%nbf=nbnd_occ
  !
  allocate(Xen%E(Xen%nb,nkibz,n_sp_pol))
  allocate(Xen%f(Xen%nb,nkibz,n_sp_pol))
  !
  do is = 1, n_sp_pol
  do ik = 1, nkibz
  do ib = 1, Xen%nb
     k_plus_g(:) = k_pt(ik,:) + g_vec(ib,:)
     call c2a(v_in=k_plus_g,v_out=v_cc,mode="ki2c")
     Xen%E(ib,ik,is)=dot_product(v_cc,v_cc)
  enddo
  enddo
  enddo
  !
  Xen%f(1:nbnd_occ,:,:) = spin_occ
  Xen%f(nbnd_occ+1,:,:) = 0._SP
  !
  ! Xw
  !
  Xw%n_freqs=X_nfreq
  allocate(Xw%p( Xw%n_freqs) )

  if (l_write_output) write(*,*)
  !
end subroutine main_vars_init
!
!
subroutine io_get_line_data(iun,label,line,ind,ierr)
  !
  ! iun:   file unit
  ! label: data label
  ! line:  if label is found, contains the line with label (empty otherwise)
  ! ind:   if label is found, index of the first character after ":" in the line
  !        (0 otherwise)
  ! ierr:  =0  success,  /=0 fail
  !
  implicit none
  integer,      intent(in)  :: iun 
  character(*), intent(in)  :: label
  character(*), intent(out) :: line
  integer,      intent(out) :: ind, ierr

  logical :: l_eof, lfound

  ierr=0
  ind=0
  line=""
  l_eof=.false.
  lfound=.false.
  !
  line_loop: &
  do while (.true.)
    !
    read(iun,fmt="(A256)",END=10,iostat=ierr) line
    go to 20
    !
10  if (l_eof) then 
      ierr=1 
      return
    else
      l_eof=.true.
      rewind(iun)
    endif
    !
20  continue
    if (ierr/=0) return
    !
    if (index(line,trim(label))>0 ) lfound=.true.
    !
    if (lfound) then
      ind=index(line,":")+1  
      return
    endif
    ! 
  enddo line_loop

end subroutine io_get_line_data
