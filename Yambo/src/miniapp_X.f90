 
#include <dev_defs.h>

program miniapp_X
  !
  use stderr,       ONLY:intc,l_write_output,debug_level
  use DIPOLES,      ONLY:DIPOLE_t!,DIP_alloc
  use X_m,          ONLY:X_t
  use X_m_int,      ONLY:X_irredux
  use electrons,    ONLY:levels
  use frequency,    ONLY:w_samp
  use R_lattice,    ONLY:bz_samp
  use matrix,       ONLY:PAR_matrix
  use timing_m,     ONLY:timing,timing_upto_now,l_use_timing
  use parallel_m,   ONLY:l_yminiapp_par
  !use parallel_int, ONLY:PARALLEL_global_indexes
  use iso_fortran_env, ONLY: stdout => output_unit
  !
  implicit none
  !
  type(levels)  :: Xen
  type(bz_samp) :: Xk,q
  type(X_t)     :: X
  type(DIPOLE_t):: Dip
  type(w_samp)  :: Xw
  type(PAR_matrix) :: X_par
  integer          :: iq
  character(256)   :: filedat 

  l_use_timing=.true.
  l_write_output=.true.
  debug_level=0
  !
  ! global init
  call init_global("miniapp_X")
  !
  call parse_cmd_line("miniapp_X",filedat)
  if(l_write_output) write(*,*) "filedat: ", filedat

  !
  ! Setup dimensions
  !
  if(l_use_timing) call timing("initializations",OPR="start")
  if(l_write_output) call headers(stdout,"Setting variables","major")
  !
  ! general variables
  call main_vars_init(Xen,Xk,Xw,filedat)
  !
  ! X-specific variables
  call X_pre_setup(Xen,Xk,X,X_par)
  call X_ALLOC_parallel(X_par,X%ng,Xw%n_freqs,"XUP")
  !
  ! DIPOLE-specific variables
  call DIPOLE_init(Xen,Xk,Dip)
  !
  if(l_use_timing) then
    call timing("initializations",OPR="stop")
    call timing_upto_now()
  endif

  !
  ! Q-loop
  !
  if(l_write_output) call headers(stdout,"Computing X_irredux","major")
  if(l_use_timing) call timing("kernel",OPR="start")
  !
  do iq=X%iq(1),X%iq(2)
    !
    if(l_write_output) then
      write(*,*)
      call headers(stdout,"Computing X_irredux iq ="//trim(intc(iq)),"minor")
    endif
    !
    if (l_yminiapp_par) then
      !
      ! PARALLEL indexes
      !==================
      !call PARALLEL_global_indexes(Xen,Xk,q,"Response_G_space",X=X)
      !
      ! WF distribution 
      !=================
      !call PARALLEL_WF_distribute(K_index=PAR_IND_Xk_ibz,B_index=PAR_IND_CON_BANDS_X(X%whoami),&
      !&                           Bp_index=PAR_IND_VAL_BANDS_X(X%whoami),CLEAN_UP=.TRUE.)
      !
      !call PARALLEL_WF_index(COMM=PAR_COM_Q_A2A)
      !
    endif
    !
    if(debug_level==1) write(*,*) "before X_irredux "
    call X_irredux(iq,"Xo",X_par,Xen,Xk,Xw,X,Dip)
    if(debug_level==1) write(*,*) "after X_irredux "
    if(l_use_timing) call timing_upto_now()
    !
  enddo
  if(l_use_timing) call timing("kernel",OPR="stop")

  !
  ! cleanup
  !
  !call DIP_alloc('DIP_iR')
  !call X_ALLOC_elemental('X')
  
  !
  ! cleanup & finalize
  !
  call finalize_global("miniapp_X")

end program miniapp_X
