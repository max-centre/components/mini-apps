
#include <dev_defs.h>

#include <y_memory.h>

module pars
  !
  use iso_fortran_env, ONLY:int32,int64
  !
  implicit none
  !
  integer, parameter  :: SP = selected_real_kind(6,30)
  integer, parameter  :: DP = selected_real_kind(14,200)
  integer, parameter  :: IP  = int32      ! 4 bytes integer, == selected_int_kind(8)
  integer, parameter  :: IPL = int64      ! 8 bytes integer, == selected_int_kind(16)
  integer, parameter  :: LP = selected_int_kind(2)
  !
  integer, parameter  :: schlen=100
  integer, parameter  :: lchlen=300
  !
  !...PARALLEL
  integer, parameter :: max_n_of_cpus=100000
  integer, parameter :: MAX_N_GROUPS   =100  ! MAX number of groups
  integer, parameter :: MAX_N_OF_CHAINS=100
  integer, parameter :: n_CPU_str_max=20
  !
  !...MEMORY
  integer, parameter :: N_MEM_max                     = 1000
  integer, parameter :: N_MEM_max_element_components  = 600
  integer, parameter :: N_MEM_SAVE_max      = 500
  !
  !...MEMORY library
  integer, parameter :: N_MEM_LIBs_max   = 200
  integer, parameter :: N_MEM_shelf_max  = 200
  !
  complex(SP), parameter :: cZERO=(0._SP,0._SP)
  complex(SP), parameter :: cONE =(1._SP,0._SP)
  complex(SP), parameter :: cI   =(0._SP,1._SP)
  !
  real(SP),    parameter :: pi=3.141592653589793238462643383279502884197_SP
  real(SP),    parameter :: zero_dfl=1.E-5_SP
  !real(SP),    parameter :: k_rlu_zero(3)=zero_dfl
  !
end module pars
!
!
module units
 !
 use pars,       ONLY:SP
 !
 implicit none
 !
 real(SP) :: HA2EV=27.2116
 !
end module units
!
!
module gpu_m
  implicit none
#ifdef _GPU
  logical, parameter:: have_gpu=.true.
#else
  logical, parameter:: have_gpu=.false.
#endif
end module gpu_m
!
!
module stderr
  !
  use pars,    ONLY:SP,schlen
  !
  implicit none
  !
  integer :: f_format_length
  integer :: g_format_length
  !
  integer :: of_format_length
  integer :: og_format_length
  !
  logical :: l_write_output
  integer :: debug_level
  !
  contains
   !
   character(8) function intc(i)
     !
     character(8) temp
     integer, intent(in) :: i
     !
     if(i.lt.10.and.i.ge.0) then
       write(temp,'(i1)') i
     else if(i.lt.100.and.i.gt.-10) then
       write(temp,'(i2)') i
     else if(i.lt.1000.and.i.gt.-100) then
       write(temp,'(i3)') i
     else if(i.lt.10000.and.i.gt.-1000) then
       write(temp,'(i4)') i
     else if(i.lt.100000.and.i.gt.-10000) then
       write(temp,'(i5)') i
     else if(i.lt.1000000.and.i.gt.-100000) then
       write(temp,'(i6)') i
     else if(i.lt.10000000.and.i.gt.-1000000) then
       write(temp,'(i7)') i
     else if(i.lt.100000000.and.i.gt.-10000000) then
       write(temp,'(i8)') i
     else
       write(temp,'(a6)') "******"
     endif
     intc = temp
     !
   end function intc
   !
   character(schlen) function real2ch(r)
     !
     real(SP), intent(in) :: r
     character(schlen)    :: fmt_
     !
     fmt_=gen_fmt(r_v=(/r/))
     write(real2ch,'('//trim(fmt_)//')') r
     !
   end function real2ch
   !
   character(schlen) function gen_fmt(i_v,r_v,l_omode)
     integer ,    optional :: i_v(:)
     real(SP),    optional :: r_v(:)
     logical,     optional :: l_omode
     !
     ! Work Space
     !
     integer  :: f_length,g_length
     logical  :: l_omode_
     integer  :: MXexp,MNexp,MDexp,iexp,iexp_pos,MXval,i1,G_shift
     real(SP) :: MX,MN,abs_r_v
     !
     l_omode_=.false.
     if(present(l_omode)) l_omode_=l_omode
     !
     if (present(i_v)) then
       MXval=max(maxval(i_v),-minval(i_v))
       iexp=1
       if (MXval/=0) iexp=nint(log10(real(MXval)))+2
       write (gen_fmt,'(a,i2.2)')  'i',iexp
     endif
     !
     if (present(r_v)) then
       MN= huge(1._SP)
       MX=-huge(1._SP)
       do i1=1,size(r_v)
         abs_r_v=abs(r_v(i1))
         if (abs_r_v<MN.and.abs_r_v>0._SP) MN=abs_r_v
         if (abs_r_v>MX.and.abs_r_v>0._SP) MX=abs_r_v
       enddo
       if (all((/r_v(:)==0._SP/))) then
         iexp=0
         iexp_pos=0
         MDexp=0
       else
         MXexp=int(log10(MX))
         MNexp=int(log10(MN))
         iexp=max(iabs(MXexp),iabs(MNexp))
         iexp_pos=max(MXexp,0)
         MDexp=int(log10(MX/MN))
       endif
       !
       g_length=g_format_length
       f_length=f_format_length
       !
       if (l_omode_) then
         g_length=og_format_length
         f_length=of_format_length
       endif
       !
       if (iexp<=2.and.MDexp<=2.and..not.l_omode_) then
         !
         !  f_format_length-3-iexp:  3 for '-','.' + 1 as iexp(10)=1/iexp(100)=2
         !
         write (gen_fmt,'(2(a,i2.2))')  'F',f_length,'.',f_length-3-iexp_pos
         !
       else
         !
         if (iexp< 9)  G_shift=6   !Gx.x-6E1  g_length-6: 6 because of '-.','E-0'   +1 for leading number
         if (iexp>=9)  G_shift=7   !Gx.x-7E2  g_length-7: 7 because of '-.','E-00'  +1 for leading number
         if (iexp>=99) G_shift=8   !Gx.x-8E3  g_length-8: 8 because of '-.','E-000' +1 for leading number
         !
         if (iexp< 9)  write (gen_fmt,'(2(a,i2.2),a)')  'G',g_length,'.',g_length-G_shift,'E1'
         if (iexp>=9)  write (gen_fmt,'(2(a,i2.2),a)')  'G',g_length,'.',g_length-G_shift,'E2'
         if (iexp>=99) write (gen_fmt,'(2(a,i2.2),a)')  'G',g_length,'.',g_length-G_shift,'E3' 
         !
       endif
       !
     endif
     !
  end function
   !
end module stderr
!
!
module electrons
  !
  use pars,   ONLY:SP
  !
  implicit none
  !
  real(SP) :: nel
  real(SP) :: default_nel
  real(SP) :: spin_occ
  real(SP) :: filled_tresh
  integer  :: n_bands
  integer  :: n_met_bands(2)
  integer  :: n_full_bands(2)
  !
  integer  :: n_spin
  integer  :: n_sp_pol
  integer  :: n_spinor
  integer  :: n_spin_den
  logical  :: l_spin_orbit
  !
  ! Levels 
  !--------
  type levels
   !
   integer         :: nb                 ! Number of bands
   integer         :: nk                 ! Numbef of k-points
   integer         :: nbf(2)             ! 1-2 refers to spin polarizations
   integer         :: nbm(2)
   integer         :: nbc(2)             ! Number of bands with NEQ carriers for yambo_rt
   real(SP)        :: E_Fermi
   real(SP)        :: E_dir_gap(2)       ! 1-2 refers to spin polarizations
   real(SP)        :: E_ind_gap(2)
   real(SP)        :: E_VBM(2)           ! Valence    Band Maximum
   real(SP)        :: E_CBm(2)           ! Conduction Band minimum
   !
   ! QP corrections
   !
   real(SP),   allocatable :: E(:,:,:)        ! Kohn-Sham or Quasi-particle energies 
   real(SP),   allocatable :: Eo(:,:,:)       ! Bare energies (Kohn-Sham) 
   real(SP),   allocatable :: f(:,:,:)        ! Electronic occupation 
   real(SP),   allocatable :: fo(:,:,:)       ! Bare electronic occupation 
   real(SP),   allocatable :: df(:,:,:)        
   real(SP),   allocatable :: W(:,:,:)        ! Imaginary part of the quasi-particle 
   complex(SP),allocatable :: Z(:,:,:)        ! Renormalization factor 
   !
   ! Green Functions
   !
   logical                 :: GreenF_is_causal
   integer                 :: GreenF_n_E_steps
   complex(SP),allocatable :: GreenF_W(:,:,:,:)   
   complex(SP),allocatable :: GreenF(:,:,:,:)     
   ! 
   ! Fine grid energy and k-points grid
   !
   !type (E_fine_grid) :: FG
   !   
   ! Perturbative SOC
   !
   integer              :: nb_SOC
   integer,    allocatable :: table_SOC(:,:,:)
   real(SP),   allocatable :: E_SOC(:,:)           
   !
  end type levels
  !
end module electrons
!
!
module frequency
  !
  use pars,   ONLY:SP
  !
  implicit none
  !
  ! W samp
  !--------
  integer :: coarse_grid_N
  real(SP),allocatable :: coarse_grid_Pt(:)
  integer, allocatable :: ordered_grid_index(:)
  integer, allocatable :: coarse_grid_index(:)
  integer, allocatable :: bare_grid_N(:)
  !
  type w_samp
   integer      :: n_freqs
   real(SP)     :: er(2)
   real(SP)     :: ir(2)
   real(SP)     :: damp_reference
   real(SP)     :: dr(2)
   real(SP)     :: per_memstps
   complex(SP),allocatable :: p(:)
   character(2) :: samp_type
   character(2) :: samp_grid
   character(2) :: mpa_solver
   character(16):: grid_type
  end type w_samp
  !
end module frequency
!
!
module R_lattice
  !
  use pars,   ONLY:SP,schlen
  !
  implicit none
  !
  integer, allocatable :: qindx_X(:,:,:)
  integer, allocatable :: qindx_B(:,:,:)
  integer, allocatable :: qindx_S(:,:,:)
  integer, allocatable :: qindx_C(:,:,:)
  !
  ! SYMs
  !
  real(SP),allocatable :: rl_sop(:,:,:)  ! Symmetry matrices in reciprocal space
  !
  ! RL lattice
  !
  integer              :: n_g_shells     ! Number of G-shells
  integer              :: ng_vec         ! Number of G-vectors
  integer              :: ng_closed      ! Number of G closed
  integer ,allocatable :: ng_in_shell(:) ! Number of G in each shell
  integer ,allocatable :: g_rot(:,:)
  integer ,allocatable :: G_m_G(:,:)
  integer ,allocatable :: minus_G(:)
  real(SP),allocatable :: g_vec(:,:)
  real(SP),allocatable :: E_of_shell(:)  ! Energy associated at each shell
  ! GPUs
  real(SP),allocatable DEV_ATTR :: g_vec_d(:,:)
  integer ,allocatable DEV_ATTR :: g_rot_d(:,:)
  integer ,allocatable DEV_ATTR :: G_m_G_d(:,:)
  integer ,allocatable DEV_ATTR :: minus_G_d(:)
  !
  ! Q/K-sampling
  !
  integer  :: nqbz
  integer  :: nqibz
  integer  :: nkbz
  integer  :: nkibz
  integer  :: nXkbz
  integer  :: nXkibz
  real(SP) :: RL_vol
  real(SP) :: d3k_factor
  real(SP) :: d3q_factor
  real(SP) :: b(3,3)
  real(SP) :: q0_def_norm, q0_shift_norm, smallest_q_norm
  real(SP),allocatable :: q_norm(:)
  character(schlen)    :: q_source="is User defined and/or read from the database"
  !
  ! Q/K sampling shadow tables
  !
  real(SP),allocatable :: k_pt(:,:)
  real(SP),allocatable :: q_pt(:,:)
  integer ,allocatable :: q_sstar(:,:)
  !
  ! BZ samp
  !---------
  !
  type bz_samp
   !
   ! Coarse grid
   !
   integer           :: nibz
   integer           :: nbz
   integer,  allocatable :: nstar(:)      ! n° of points in the star
   integer,  allocatable :: star(:,:)     ! ik,ikstar --> is     is sends ik in ikstar
   integer,  allocatable :: sstar(:,:)    ! ik_bz     --> ik,is  is sends ik in ikbz
   integer,  allocatable :: s_table(:,:)  ! ik,is     --> sym@k_bz  revers of sstar(2)
   integer,  allocatable :: k_table(:,:)  ! ik,is     --> ik_bz     revers of sstar(1)
   real(SP), allocatable :: pt(:,:)      
   real(SP), allocatable :: ptbz(:,:)    
   real(SP), allocatable :: weights(:)   
   character(6)      :: description
   character(1)      :: units
   !
   ! Fine grids(s)
   !
   !type (bz_fine_grid) :: FGbare
   !type (bz_fine_grid) :: FGibz
   !type (bz_fine_grid) :: FGbz
   !!
   !real(SP), allocatable :: weights_ipol(:)           ! something used by mapping with wannier
   !                                               
  end type bz_samp
  !
end module R_lattice
!
!
module D_lattice
 !
 use pars,        ONLY:SP,lchlen,cONE,cZERO,cI,schlen
 use gpu_m,      ONLY:have_gpu
 !
 implicit none
 !
 ! Non periodic directions
 !
 character(lchlen) :: non_periodic_directions
 real(SP)          :: molecule_position(3)
 !
 !
 ! Temperature
 !
 real(SP) :: Tel
 real(SP) :: T_elecs
 real(SP) :: T_holes
 real(SP) :: Bose_Temp
 real(SP) :: input_GS_Tel
 logical  :: input_Tel_is_negative
 !
 ! SYMs
 !
 integer :: nsym
 integer :: i_time_rev  ! =1 yes TR , =0 no TR
 integer :: i_space_inv ! =1 yes SI,  =0 no SI
 integer :: inv_index   ! Index of the symmetry corresponding to -I (independently 
                        ! on the value of i_time_rev and i_space_inv
 integer :: idt_index   ! Indentity I  index
 logical :: mag_syms
 real(SP),allocatable    :: dl_sop(:,:,:)   ! Symmetry operation in real-space
 complex(SP),allocatable :: spin_sop(:,:,:)
 integer, allocatable    :: sop_tab(:,:)
 integer, allocatable    :: sop_inv(:)
 integer, allocatable    :: irt(:,:,:)     ! (nsym,nat,n_at_species) gives the index of a rotated atomic position
 integer, allocatable    :: icell(:,:,:,:) ! (3,nsym,nat,n_at_species) gives the referene cell after a symm is operated
 ! device memory
 complex(SP),allocatable DEV_ATTR :: spin_sop_d(:,:,:)
 integer,    allocatable DEV_ATTR :: sop_inv_d(:)
 !
 ! Cell & atoms
 !
 logical  :: l_0D,l_1D,l_2D,l_3D
 integer  :: n_atoms
 integer  :: n_atoms_species_max
 integer  :: n_atomic_species
 real(SP) :: DL_vol
 real(SP) :: a(3,3)
 real(SP) :: alat(3)
 real(SP), allocatable :: atom_mass(:)
 integer,  allocatable :: atoms_map(:,:)  ! Map atoms order from YAMBO to the DFT one
 integer,  allocatable :: PW_atomic_kind(:)
 integer,  allocatable :: n_atoms_species(:)
 integer,  allocatable :: Z_species(:)
 real(SP), allocatable :: atom_pos(:,:,:)
 character(lchlen)     :: atoms_string
 character(schlen)     :: lattice
 !
 contains
   !
   subroutine symmetry_group_table()
     !   
     use pars,           ONLY:SP,IP
     use stderr,         ONLY:l_write_output
     implicit none
     !   
     ! Work Space
     !   
     integer  :: i1,i2,i3
     real(SP) :: m(3,3)
     !   
     YAMBO_FREE(sop_tab)
     YAMBO_FREE(sop_inv)
     YAMBO_ALLOC(sop_tab,(nsym,nsym))
     YAMBO_ALLOC(sop_inv,(nsym))
     sop_tab=0
     sop_inv=0
     do i1=1,nsym
       do i2=1,nsym
         m=matmul(dl_sop(:,:,i1),dl_sop(:,:,i2))
         do i3=1,nsym
           if (all(abs(m-dl_sop(:,:,i3))<=1.E-5)) then
             if (sop_tab(i1,i2)/=0) call error('[SYMs] check the input symmetries!')    
             sop_tab(i1,i2)=i3
             if (sop_tab(i1,i2)==1) sop_inv(i1)=i2    
           endif
         enddo
         if (sop_tab(i1,i2)==0) call error('[SYMs] check the input symmetries!')    
       enddo
     enddo
     if (any(sop_inv==0)) call error('[SYMs] check the input symmetries!')
     if(l_write_output) write(*,*) 'Group table correct   '
     !   
#if defined _GPU
     if (have_cuda) then
       YAMBO_FREE(sop_inv_d)
       YAMBO_ALLOC_SOURCE(sop_inv_d,sop_inv)
     endif
#endif
     !   
   end subroutine symmetry_group_table

end module D_lattice
!
!
module matrix_operate
 !
 use pars, ONLY:SP
 !
 implicit none
 !
 contains
   !
   function m3det(m)
     real(SP) :: m(3,3),m3det
     m3det = m(1,1)*( m(2,2)*m(3,3) - m(2,3)*m(3,2) )   &
&           -m(1,2)*( m(2,1)*m(3,3) - m(2,3)*m(3,1) ) + &
&            m(1,3)*( m(2,1)*m(3,2) - m(2,2)*m(3,1) )
   end function
   !
   subroutine m3inv(m,m_inv)
     !
     ! Analitic inverse of a real 3x3 matrix
     !
     real(SP)          :: m(3,3)
     real(SP),optional :: m_inv(3,3)
     real(SP)          :: det,inv(3,3) ! Work Space
     det=m3det(m)
     inv(1,1)=(-m(2,3)*m(3,2) + m(2,2)*m(3,3) )/det
     inv(1,2)=( m(1,3)*m(3,2) - m(1,2)*m(3,3) )/det
     inv(1,3)=(-m(1,3)*m(2,2) + m(1,2)*m(2,3) )/det
     inv(2,1)=( m(2,3)*m(3,1) - m(2,1)*m(3,3) )/det
     inv(2,2)=(-m(1,3)*m(3,1) + m(1,1)*m(3,3) )/det
     inv(2,3)=( m(1,3)*m(2,1) - m(1,1)*m(2,3) )/det
     inv(3,1)=(-m(2,2)*m(3,1) + m(2,1)*m(3,2) )/det
     inv(3,2)=( m(1,2)*m(3,1) - m(1,1)*m(3,2) )/det
     inv(3,3)=(-m(1,2)*m(2,1) + m(1,1)*m(2,2) )/det
     !
     if (abs(det)<=1.E-7) call error('Null determinant. Inversion failed.')
     if (present(m_inv))      m_inv=inv
     if (.not.present(m_inv)) m=inv
     !
   end subroutine
   !
end module matrix_operate
!
!
module vec_operate
 !
 use pars,      ONLY:SP,zero_dfl,pi
 !
 implicit none
 !
 contains
   !
   pure real(SP) function v_norm(v)
     real(SP), intent(in) :: v(3)
     v_norm=sqrt(dot_product(v,v))
   end function v_norm
   !
   function cross_product(a,b)
     real(SP), intent(in) :: a(3),b(3)
     real(SP)             :: cross_product(3)
     cross_product(1) = a(2) * b(3) - a(3) * b(2)
     cross_product(2) = a(3) * b(1) - a(1) * b(3)
     cross_product(3) = a(1) * b(2) - a(2) * b(1)
   end function cross_product
   !
   subroutine sort(arrin,arrout,indx,indx_m1,r_zero)                             
     !
     ! Sort real(dt) values from arrin into array 
     ! arrout and give permutations in indx, indx_m1.
     ! Content of indx is destroyed.
     ! indx_m1: j went to   position indx_m1(j)
     ! indx   : i came from position indx(i) 
     !
     real(SP)::  arrin(:)
     real(SP), optional::  arrout(:)
     integer,  optional::  indx(:)
     integer,  optional::  indx_m1(:)
     real(SP), optional :: r_zero
     !
     ! local variables
     !
     integer  :: j, i,n, ir, l, indxt
     real(SP) :: q,r_zero_
     integer, allocatable:: l_indx(:)
     real(SP),allocatable:: l_arrout(:)
     !
     r_zero_=0._SP
     if(present(r_zero)) r_zero_=r_zero
     !
     n=size(arrin)
     allocate(l_indx(n),l_arrout(n))
     !
     if(n.eq.1) then
       l_arrout(1) = arrin(1)
       l_indx(1) = 1
       if (present(arrout)) arrout=l_arrout
       if (.not.present(arrout)) arrin=l_arrout
       if (present(indx)) indx=l_indx
       deallocate(l_indx,l_arrout)
       return
     endif
     do j=1,n
       l_indx(j)=j
     enddo
     l=n/2+1
     ir=n
  1  continue
     if (l.gt.1)then
       l=l-1
       indxt=l_indx(l)
       q=arrin(indxt)
     else
       indxt=l_indx(ir)
       q=arrin(indxt)
       l_indx(ir)=l_indx(1)
       ir=ir-1
       if (ir.eq.1)then
         l_indx(1)=indxt
         go to 3
       endif
     endif
     i=l
     j=l+l
  2  if (j.le.ir)then
      if (j.lt.ir) then
        if (arrin(l_indx(j))<arrin(l_indx(j+1))-r_zero_) j=j+1
      endif
      if (q<arrin(l_indx(j))-r_zero_) then
        l_indx(i)=l_indx(j)
        i=j
        j=j+j
      else
        j=ir+1
      endif
      go to 2
     endif
     l_indx(i)=indxt
     go to 1
  3  continue
     do i=1,n
       l_arrout(i) = arrin(l_indx(i))
     enddo
     if (present(arrout)) arrout=l_arrout
     if (.not.present(arrout)) arrin=l_arrout
     if (present(indx)) indx=l_indx
     if (present(indx_m1)) forall( i=1:n) indx_m1(l_indx(i))=i
     deallocate(l_indx,l_arrout)
   end subroutine sort
   !
   subroutine define_b_and_DL_vol(a,b,DL_vol)
     !
     use pars,        ONLY:pi
     !
     real(SP), intent(in)  :: a(3,3)
     real(SP), intent(out) :: b(3,3),DL_vol
     !
     real(SP)  :: cp(3)
     integer   :: i1
     !
     cp = cross_product(a(2,:),a(3,:))
     DL_vol=0._SP
     do i1=1,3
       DL_vol= DL_vol+a(1,i1)*cp(i1)
     enddo
     b(1,:)=cross_product(a(2,:),a(3,:))*2.0_SP*pi/DL_vol
     b(2,:)=cross_product(a(3,:),a(1,:))*2.0_SP*pi/DL_vol
     b(3,:)=cross_product(a(1,:),a(2,:))*2.0_SP*pi/DL_vol
     !b(1,:)=cross_product(a(2,:),a(3,:))/DL_vol
     !b(2,:)=cross_product(a(3,:),a(1,:))/DL_vol
     !b(3,:)=cross_product(a(1,:),a(2,:))/DL_vol
     if ( DL_vol <0._SP) then
       write(*,*) "Axis vectors are left handed"
       DL_vol=abs(DL_vol)
     endif
     !
   end subroutine define_b_and_DL_vol
   !
   !
   pure logical function v_is_zero(v,zero_)
     real(SP), intent(in) :: v(3)
     real(SP), intent(in), optional :: zero_(3)
     real(SP)          :: local_zero(3)
     !
     local_zero=zero_dfl
     if (present(zero_)) local_zero=zero_
     !
     v_is_zero=all((/abs(v(1))<=local_zero(1),&
&                    abs(v(2))<=local_zero(2),&
&                    abs(v(3))<=local_zero(3)/))
   end function v_is_zero
   !
   pure logical function rlu_v_is_zero(v,zero_)
     real(SP), intent(in), optional :: zero_(3)
     real(SP), intent(in)           :: v(3)
     real(SP)          :: u(3)
     u=v-nint(v)
     if (.not.present(zero_)) rlu_v_is_zero=v_is_zero(u)
     if (     present(zero_)) rlu_v_is_zero=v_is_zero(u,zero_)
   end function rlu_v_is_zero
   !
   subroutine k2bz(v_in,v_out,b_in) 
     !
     ! k is iku 
     !
     use R_lattice,  ONLY:b
     real(SP)           :: v_in(3)
     real(SP), optional :: v_out(3),b_in(3,3)
     ! 
     ! Work Space
     ! 
     real(SP):: b_here(3,3),p(3),q(3),u(3),dist
     integer :: i1,i2,i3
     integer,parameter :: ni=2
     !
     if (present(b_in))      b_here=b_in
     if (.not.present(b_in)) b_here=b
     !
     call c2a(b_here,v_in,q,'ki2a')
     call c2a(b_here,v_in,p,'ki2c')
     dist=v_norm(p)
     do i1=-ni,ni
       do i2=-ni,ni
         do i3=-ni,ni
           call c2a(b_here,q(:)-(/i1,i2,i3/),u,'ka2c')
           if (v_norm(u)<dist-1.E-5) p=u
           if (v_norm(u)<dist-1.E-5) dist=v_norm(u)
         enddo
       enddo
     enddo
     call c2a(b_here,p,q,'kc2i')
     !
     if (present(v_out))      v_out=q
     if (.not.present(v_out)) v_in=q
     !
   end subroutine k2bz
   !
   subroutine c2a(b_in,v_in,v_out,mode)
     !
     ! rlu = Reduced Lattice Units (crystal coordinated in Quantum-Espresso)
     ! cc  = Cartesian Coordinates (atomic units)
     ! iku = Internal K-Units (internal Yambo units similar to 2pi/Alat in QE but with 3 scaling factors)
     ! 
     ! Nota bene: "a" stands for "rlu" for no obvious reason  
     !
     ! mode = 'k/r c2a' 'k/r a2c' (cc  <-> rlu)
     ! mode = 'k/r i2a' 'k/r a2i' (iku <-> rlu)
     ! mode = 'k/r i2c' 'k/r c2i' (cc  <-> iku)
     !
     use D_lattice,      ONLY:alat,a
     use R_lattice,      ONLY:b
     use matrix_operate, ONLY:m3inv
     real(SP)          :: v_in(3)
     real(SP), optional:: b_in(3,3),v_out(3)
     character(4)::mode
     !
     ! Work Space
     !
     real(SP) a_here(3,3),mat(3,3),n(3),u(3)
     !
     ! Define local unit cell vectors
     !
     if (index(mode,'r')/=0) then
       a_here=a
       if (present(b_in)) a_here=b_in
     else
       a_here=b
       if (present(b_in)) a_here=b_in
     endif
     !
     ! Scale factor if input vector is in iku
     !
     if (index(mode,'r')/=0) n(:)=alat(:)
     if (index(mode,'k')/=0) n(:)=2._SP*pi/alat(:)
     !
     ! u is rlu or cc (no iku)
     !
     u=v_in
     if (index(mode,'i2')/=0) u(:)=v_in(:)*n(:) ! iku -> cc
     !
     ! i2c/c2i
     mat=reshape((/1._SP,0._SP,0._SP,0._SP,1._SP,0._SP,0._SP,0._SP,1._SP/),(/3,3/))
     !
     ! a2c/a2i
     if (index(mode,'a2c')/=0.or.index(mode,'a2i')/=0) mat=transpose(a_here)
     !
     ! c2a/i2a
     if (index(mode,'c2a')/=0.or.index(mode,'i2a')/=0) call m3inv(transpose(a_here),mat) 
     !
     if (present(v_out)) then
       v_out=matmul(mat,u) 
       if (index(mode,'2i')/=0) v_out(:)=v_out(:)/n(:) ! * -> iku
     else
       v_in=matmul(mat,u) 
       if (index(mode,'2i')/=0) v_in(:)=v_in(:)/n(:) ! * -> iku
     endif
   end subroutine c2a
   !
   subroutine degeneration_finder(E,n,first_deg_el,deg_elmnts,deg_grps,&
&                                 deg_accuracy,Include_single_values)
     !
     ! Assumes energy are sorted
     !
     ! E = ( 0 1 1 2 3 3 3 ) , n = 7
     !
     ! first_deg_el = ( 2 5 0 0 0 0 0 )
     ! deg_elmnts =   ( 1 2 0 0 0 0 0 )
     !
     ! deg_grps = 2
     !
     integer, intent(in)  :: n
     integer, intent(out) :: first_deg_el(n),deg_elmnts(n),deg_grps
     real(SP),intent(in)  :: E(n),deg_accuracy
     logical, optional    :: Include_single_values
     !
     ! Work Space
     !
     integer :: iref,i1
     real(SP):: E_diff(2)
     logical :: l_flag,l_singles
     !
     deg_grps=0
     first_deg_el=0
     deg_elmnts=0
     !
     l_singles=.FALSE.
     if (present(Include_single_values)) l_singles=Include_single_values
     !
     iref=1
     !
     do while (iref < n ) 
       !
       if (l_singles) then
         !
         E_diff=2._SP*deg_accuracy
         !
         if (deg_grps>0) E_diff(1)=abs(E(iref)-E(first_deg_el(deg_grps)))
         if (iref<n    ) E_diff(2)=abs(E(iref)-E(iref+1))
         !
         if (all(E_diff>=deg_accuracy)) then
           deg_grps=deg_grps+1
           first_deg_el(deg_grps)=iref
           deg_elmnts(deg_grps)=1
         endif
         !
       endif
       !
       l_flag=.true.
       !
       do i1=iref+1,n
         !
         if (abs(E(iref)-E(i1))<deg_accuracy) then
           if (l_flag) then
             deg_grps=deg_grps+1
             first_deg_el(deg_grps)=iref
             deg_elmnts(deg_grps)=1
             l_flag=.false.
           endif
           deg_elmnts(deg_grps)=deg_elmnts(deg_grps)+1
         else
           exit
         endif
       enddo
       ! 
       if (     l_flag) iref=iref+1
       if (.not.l_flag) iref=iref+deg_elmnts(deg_grps)
       !
     enddo
     !
     if(deg_grps> 0) i1=first_deg_el(deg_grps)
     if(deg_grps==0) i1=1
     !
     if(l_singles .and.  abs(E(n)-E(i1))>deg_accuracy) then
       deg_grps=deg_grps+1
       first_deg_el(deg_grps)=n
       deg_elmnts(deg_grps)=1
     endif
     !
   end subroutine
   !
   real(SP) function iku_v_norm(v,inside_bz)
     !   
     ! shifted in the BZ if inside_bz is set to true, ak2bz=|k|
     !   
     use D_lattice,      ONLY:alat
     real(SP), intent(in)           :: v(3)
     logical,  intent(in), optional :: inside_bz
     !   
     real(SP) :: u(3)
     logical  :: inside_bz_
     !   
     inside_bz_=.false.
     if(present(inside_bz)) inside_bz_=inside_bz
     !   
     u(:)=v(:)
     if(inside_bz_) call k2bz(v_in=u)
     !   
     u(:)=u(:)*2._SP*pi/alat(:)
     iku_v_norm=sqrt(dot_product(u,u))
     !   
   end function iku_v_norm
   !
end module vec_operate
!
!
module wrapper
  !
  use pars,   ONLY:SP,schlen
  !
  implicit none
  !
  interface V_plus_alpha_V
   module procedure vv_s,vv_c,vv_saxpy,vv_caxpy !,MM_caxpy
#ifdef _GPU
   module procedure          vv_c_gpu,vv_caxpy_gpu
#endif
 end interface
 !
 public :: V_plus_alpha_V
 !
 contains
   !
   !=========================
   ! interface V_plus_alpha_V
   !=========================
   !
   subroutine vv_c(N, CA, CX,  CY )
     complex(SP), intent(in) :: CA
     integer,     intent(in) :: N
     complex(SP), intent(in) :: CX(*)
     complex(SP), intent(out):: CY(*)
     call CAXPY(N,CA,CX,1,CY,1)
   end subroutine vv_c
   !
   subroutine vv_caxpy(N, CA, CX, LDA,  CY, LDB )
     complex(SP), intent(in) :: CA
     integer,     intent(in) :: N
     complex(SP), intent(in) :: CX(*)
     complex(SP), intent(out):: CY(*)
     integer,     intent(in) :: LDA,LDB
     call CAXPY(N,CA,CX,LDA,CY,LDB)
   end subroutine vv_caxpy
   !
#ifdef _GPU
   subroutine vv_c_gpu(N, CA, CX, CY )
     complex(SP), intent(in) :: CA
     integer,     intent(in) :: N
     complex(SP), device, intent(in) :: CX(*)
     complex(SP), device, intent(out):: CY(*)
     call cublasCaxpy(N,CA,CX,1,CY,1)
   end subroutine vv_c_gpu
#endif
   !
#ifdef _GPU
   subroutine vv_caxpy_gpu(N, CA, CX, LDA, CY, LDB )
     complex(SP), intent(in) :: CA
     integer,     intent(in) :: N
     complex(SP), device, intent(in) :: CX(*)
     complex(SP), device, intent(out):: CY(*)
     integer,     intent(in) :: LDA,LDB
     call cublasCaxpy(N,CA,CX,LDA,CY,LDB)
   end subroutine vv_caxpy_gpu
#endif
   !
   subroutine vv_s(N, CA, CX, CY )
     real(SP),    intent(in) :: CA
     integer,     intent(in) :: N
     real(SP),    intent(in) :: CX(*)
     real(SP),    intent(out):: CY(*)
     call SAXPY(N,CA,CX,1,CY,1)
   end subroutine vv_s   
   !
   subroutine vv_saxpy(N, CA, LDA, CX, CY, LDB )
     real(SP),    intent(in) :: CA
     integer,     intent(in) :: N
     real(SP),    intent(in) :: CX(*)
     real(SP),    intent(out):: CY(*)
     integer,     intent(in) :: LDA,LDB
     call SAXPY(N,CA,CX,LDA,CY,LDB)
   end subroutine vv_saxpy   
 !
end module wrapper
!
!
module wave_func
 !
 use pars,           ONLY:SP
 !
 implicit none
 !
 integer                :: wf_ncx
 integer                :: wf_ng
 !
 type WAVEs 
   integer              :: b(2)      ! band range 
   integer              :: k(2)      ! k range
   integer              :: sp_pol(2) ! sp_pol_range
   integer              :: N         ! states 
   character(1)         :: space
   complex(SP), allocatable :: c(:,:,:)
   integer    , allocatable :: index(:,:,:)
   logical    , allocatable :: state(:,:,:)
   logical                  :: to_load = .TRUE.
   complex(SP), allocatable DEV_ATTR :: c_d(:,:,:)
 end type WAVEs
 !
 ! Buffered WFs
 !
 type (WAVEs), save :: WF_buffer
 !
 ! Actual WFs
 !
 type (WAVEs), save, target :: WF
 !
end module wave_func
!
!#ifdef _MKLGPU
!include "mkl_dfti_omp_offload.f90"
!#endif

module FFT_m
 use pars,          ONLY: SP
 use iso_c_binding, ONLY: c_ptr
#ifdef _MKLGPU
 use mklfft_gpu
#endif
 !
 implicit none
 !
 real(SP)             :: fft_norm
 integer              :: fft_size
 integer              :: fft_dim(3)
 integer              :: fft_multiplier(3)
 integer              :: fft_dim_loaded(3)
 integer, allocatable :: fft_g_table(:,:)
 integer, allocatable :: fft_rot_r(:,:)
 integer, allocatable :: fft_rot_r_inv(:)
 integer, allocatable DEV_ATTR :: fft_g_table_d(:,:)
 integer, allocatable DEV_ATTR :: fft_rot_r_d(:,:)
 integer, allocatable DEV_ATTR :: fft_rot_r_inv_d(:)
 !
 ! FFTW support
 !
 integer(8)  :: fftw_plan  !(FW) +1 sign: G-space to R-space & oscillators
                           !(BW) -1 sign: R-space to G-space
 !
 ! cuFFT support  (CUDA)
 !
 integer     :: cufft_plan !(FW) +1 sign: G-space to R-space & oscillators
                           !(BW) -1 sign: R-space to G-space
 !
 ! hipFFT support (AMD GPUs)
 !
 type(c_ptr) :: hipfft_plan
                           !(BW) -1 sign: R-space to G-space

 contains
   !
   integer function modx(i,j)
     !
     !  Calculate a function similar to MOD:
     !  modx(i,j) = mod (i,j) i>=0
     !            = mod ( mod(i,j)+j , j) i<0
     !  Thus
     !   modx(3,2)=1
     !   modx(-1,2)=1.
     !  This is different from MOD for i negative.
     !
     implicit none
     integer i,j
     if (i>=0) then
       modx = mod(i,j)
     else
       modx = mod( mod(i,j) + j , j)
     endif
     !
   end function
   !
end module
