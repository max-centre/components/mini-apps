
subroutine build_gvec(ecutwfc,ng_vec,g_vec)
  use pars,         ONLY:SP,pi
  use stderr,       ONLY:l_write_output,debug_level
  use R_lattice,    ONLY:b
  use D_lattice,    ONLY:alat
  use vec_operate,  ONLY:sort
  implicit none
  !
  ! AF: g_vec indexes are mis-ordered for
  !     consistency with legacy code
  !
  real(SP), intent(in) :: ecutwfc
  integer,  intent(in) :: ng_vec
  real(SP), intent(inout) :: g_vec(ng_vec,3)
  !
  integer :: ig, i,j,k, nv(3), iv(3)
  integer :: ind, ndim, ngm
  real(SP):: b_cc(3,3), v(3), norm(3), gnorm
  integer,  allocatable :: igv(:,:), map(:)
  real(SP), allocatable :: gg(:), gg_sorted(:)

  ! 
  ! b in cc  [Bohr^-1]
  ! 
  b_cc = b
  !do i = 1, 3
  !   b_cc(i,:) = b_cc(i,:) * 2*pi/alat(i)
  !enddo
  if(debug_level==1) then
    write(*,*) "DEBUGGGG", alat
    write(*,"(3f15.9)") b_cc
  endif
  !
  do i = 1, 3
    norm(i) = dot_product(b_cc(i,:),b_cc(i,:))
    nv(i)   = int( sqrt(4.0_SP*ecutwfc)/sqrt(norm(i)) ) +1
  enddo
  if(debug_level==1) then
    write(*,"(3f15.9)") norm
    write(*,"(3i8)") nv
  endif

  ndim=product( 2*nv(:)+1 )
  allocate(igv(3,ndim)) 
  allocate(gg(ndim)) 
  igv=0
  gg=8*ecutwfc

  ind=0
  do k = -nv(3),nv(3)
  do j = -nv(2),nv(2)
  do i = -nv(1),nv(1)
     !
     v=i*b_cc(1,:)+j*b_cc(2,:)+k*b_cc(3,:)
     gnorm=dot_product(v,v)
     !
     if (gnorm <= 4.0_SP*ecutwfc ) then
        ind=ind+1
        igv(1,ind)=i
        igv(2,ind)=j
        igv(3,ind)=k
        gg(ind)=gnorm
     endif 
     !
  enddo
  enddo
  enddo
  !
  ngm=ind
  if(l_write_output) write(*,"(/,a21,2x,i8)") "ngm:", ngm
  if (ngm /= ng_vec) call error("Unexpected diff: ngm/=ng_vec")
  ! 
  ! sort g vectors 
  allocate(gg_sorted(ndim))
  allocate(map(ndim))
  call sort(gg,gg_sorted,indx=map)
  !
  deallocate(gg)
  allocate(gg(ngm))
  !
  do ig = 1, ngm
    gg(ig)=gg_sorted(ig)
    iv(:)=igv(:,map(ig))
     v(:)= iv(1)*b_cc(1,:) + iv(2)*b_cc(2,:) + iv(3)*b_cc(3,:)
    g_vec(ig,:) = v(:) * alat(:) / (2.0_SP*pi)
  enddo
  if(debug_level==1) then
    write(1000,"(f15.9)") gg/2.0d0
    write(*,*) "gg(ngm)", gg(ngm), dot_product(v,v)
  endif
  !
  ! cleanup
  !
  deallocate(gg_sorted)
  deallocate(gg)
  deallocate(igv)
  deallocate(map)
  
end subroutine build_gvec
