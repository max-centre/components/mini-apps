
#include<y_memory.h>
#include<dev_defs.h>

subroutine parse_cmd_line(main_name,filedat)
 use input_m
 implicit none
 character(*) :: main_name
 character(*) :: filedat

 integer :: narg,iarg
 character(256) :: str
 logical :: lfound

 lfound=.true.
 filedat=""
 nbnd_cmdline=0
 X_ng_cmdline=0
 X_nq_cmdline=0
 X_nfreq_cmdline=0
 !
 narg=command_argument_count()
 iarg=0
 do while (iarg <narg )
    iarg=iarg+1
    call get_command_argument(iarg,str)
    !
    if (index(str,"-f ")>0 .or. index(str,"-file")>0) then 
      iarg=iarg+1
      call get_command_argument(iarg,filedat)
    endif
    !
    if (index(str,"-nb ")>0 .or. index(str,"-nbnd")>0) then 
      iarg=iarg+1
      call get_command_argument(iarg,str)
      read(str,*) nbnd_cmdline
    endif
    !
    if (index(str,"-ng ")>0 .or. index(str,"-X_ng")>0) then 
      iarg=iarg+1
      call get_command_argument(iarg,str)
      read(str,*) X_ng_cmdline
    endif
    !
    if (index(str,"-nq ")>0 .or. index(str,"-X_nq")>0) then 
      iarg=iarg+1
      call get_command_argument(iarg,str)
      read(str,*) X_nq_cmdline
    endif
    !
    if (index(str,"-nw ")>0 .or. index(str,"-nfreq")>0) then 
      iarg=iarg+1
      call get_command_argument(iarg,str)
      read(str,*) X_nfreq_cmdline
    endif
    !
 enddo
 !
 if (len_trim(filedat)==0) lfound=.false.
 ! 
 if (.not. lfound) then
    write(*,"(a)") "Usage: "
    write(*,"(a)") trim(main_name) //".x  -f <filename> [ -nb <nbnd> ] [-ng <X_ng>] [-nq <X_nq>] [-nw <nfreq>] " 
    write(*,"(/,'Stopping')") 
    stop
 endif

end subroutine parse_cmd_line


subroutine error(mesg)
 !
 character(*)       :: mesg
 !
 write(*,*) "error "//mesg
 stop
 !
end subroutine
!
!
subroutine WF_load(WF,iG_in,iGo_max_in,bands_to_load,kpts_to_load)
 !
 use pars,            ONLY:SP
 use stderr,          ONLY:l_write_output
 use wave_func,       ONLY:WAVEs
 use electrons,       ONLY:n_spinor,n_sp_pol
 use fft_m,           ONLY:fft_size
 !
 implicit none
 !
 integer               :: iG_in,iGo_max_in,bands_to_load(2),kpts_to_load(2)
 type(WAVEs), target   :: WF
 !
 integer :: ir,i_spinor,i_wf,ik,ib,i_sp_pol
 real(SP), allocatable :: wf_tmp(:,:,:,:)
 !
 if (l_write_output) write(*,*) "Setting up wave-functions"
 !
 WF%b=bands_to_load
 WF%k=kpts_to_load
 WF%sp_pol=(/1,n_sp_pol/)
 !
 WF%N=(WF%b(2)-WF%b(1)+1)*(WF%k(2)-WF%k(1)+1)
 allocate(wf_tmp(fft_size,n_spinor,WF%N,2))
 !
 call random_number(wf_tmp)
 !
 allocate(WF%c(fft_size,n_spinor,WF%N))
 allocate(WF%index(WF%b(1):WF%b(2),WF%k(1):WF%k(2),n_sp_pol)) 
 !
 i_wf=0
 do i_sp_pol=1,n_sp_pol
 do ik=WF%k(1),WF%k(2)
 do ib=WF%b(1),WF%b(2)
   i_wf=i_wf+1
   WF%index(ib,ik,i_sp_pol)=i_wf
   do i_spinor=1,n_spinor
     do ir=1,fft_size
       WF%c(ir,i_spinor,i_wf)=cmplx(wf_tmp(ir,i_spinor,i_wf,1),wf_tmp(ir,i_spinor,i_wf,2),kind=SP)
     enddo
   enddo
 enddo
 enddo
 enddo
 !
 deallocate(wf_tmp)
 !
end subroutine WF_load
!
!
subroutine WF_free(WF)
 !
 use wave_func,    ONLY:WAVEs
 !
 type(WAVEs), target  :: WF
 !
#ifdef _GPU
 if (have_gpu) then
   if (.not. ( allocated(WF%c) .eqv. devxlib_mapped(DEV_VAR(WF%c)) ) ) & 
&    call error("[WF] inconsistent alloc of GPU wfcs")
 endif
#endif
 !
 if (.not.allocated(WF%c)) return
 !
#ifdef _GPU
 YAMBO_FREE_GPU(DEV_VAR(WF%c))
#endif
 YAMBO_FREE(WF%c)
 YAMBO_FREE(WF%state)
 YAMBO_FREE(WF%index)
 !
 WF%b=0
 WF%sp_pol=0
 WF%space=' '
 WF%to_load=.TRUE.
 !
end subroutine
!
!
subroutine DIPOLE_dimensions(Xen,Dip,bands,q0)
 !
 use pars,           ONLY:SP
 use stderr,         ONLY:l_write_output
 use electrons,      ONLY:levels
 use DIPOLES,        ONLY:DIPOLE_t
 use X_m,            ONLY:l_X_terminator,X_terminator_Kind
 use wave_func,      ONLY:wf_ng
 !
 implicit none
 !
 integer,       intent(in)    :: bands(2)
 type(levels),  intent(in)    :: Xen
 type(DIPOLE_t),intent(inout) :: Dip
 real(SP),      intent(in)    :: q0(3)
 !
 !
 ! NOTE: 1 - that in case we will enter here during a SC run to update the
 !           screened interaction all transitions must be considered in order to rotate <P>.
 !       2 - when using internal SC potentials oscilators must be rotated
 !           in the new basis. So all transitions are needed.
 !       3 - In real-time simulations P and P^2 are ALWAYS calculated => no band ordering
 !
 Dip%ng=wf_ng
 Dip%q0=q0
 Dip%ib=bands
 !
 ! Check if terminator is required
 !
 l_X_terminator = X_terminator_Kind/='none'
 !
 ! Set up band limits
 !
 if (Dip%bands_ordered.or.Dip%Energy_treshold<0._SP) then
   Dip%ib_lim(1)=maxval(Xen%nbm)
   Dip%ib_lim(2)=minval(Xen%nbf)+1
   if (l_X_terminator) Dip%ib_lim(2)=Dip%ib(1)
 else
   Dip%ib_lim(1)=Dip%ib(2)
   Dip%ib_lim(2)=Dip%ib(1)
 endif
 !
 if (l_write_output) then
   write(*,*) "Setting up dipole dimensions"
   write(*,*)
 endif
 !
end subroutine DIPOLE_dimensions
!
!
subroutine DIPOLE_init(Xen,Xk,Dip)
 !
 use pars,         ONLY:SP
 use DIPOLES,      ONLY:DIPOLE_t,DIPOLES_reset,DIP_alloc,DIP_iR
 use R_lattice,    ONLY:bz_samp
 use electrons,    ONLY:levels
 implicit none
 !
 type(levels),   intent(in)    :: Xen 
 type(bz_samp),  intent(in)    :: Xk
 type(DIPOLE_t), intent(inout) :: Dip
 !
 real(SP), allocatable :: raux(:,:,:,:,:)
 
 !
 ! init dims and alloc
 !
 call DIPOLES_reset(Dip)
 call DIPOLE_dimensions(Xen,Dip,[1,Xen%nb],[0.0_SP,0.0_SP,0.0_SP])
 call DIP_alloc('DIP_iR',(/3,Dip%ib_lim(2),Dip%ib(2),Dip%ib(1),Dip%ib_lim(1),Xk%nibz/))
 !
 ! random init of data
 !
 allocate(raux,mold=real(DIP_iR,SP))
 !
 call random_number(raux)
 DIP_iR=raux
 !
 deallocate(raux)
 !
end subroutine DIPOLE_init
!
!
subroutine k_expand(k)
 !
 ! Outputs:
 ! 
 !   k%star k%sstar k%nstar k%weights k%nbz
 !
 !Given the kpoints and the simmetry operations
 !this sub. calculates weights (and rangs of the point
 !groups) as well the simm. operations contained
 !in each star:
 !
 !k%star(ik,i) = is | R_is ik =!= ik  with i=1,k%nstar(ik)
 !
 !k%nstar(ik)   = # of k-points in the Star of ik
 !k%weights(ik) = k%nstar(ik) / (Sum_ik nk(ik) )  = Num. of symm / rank(ko) / Nk^BZ
 !
 !
 !2-dimension example:
 !
 !-------------------
 !|        |        |
 !|   X1   |   X2   |
 !|        |        |
 !-------------------
 !|        |        |
 !|   X3   |   ko   |
 !|        |        |
 !-------------------
 !
 !In this case we have 8 symmetries and we obtain
 !from the IRREDUCIBLE ko other 3 REDUCIBLE k-points i
 !(X1, X2, X3)
 !
 !rang(ko) = 2  (I plus inversion that sends X2 < - > X3)
 !k%nstar(ko) = Num. of symm / rank(ko) = 8/2 = 4 (points in the star)
 !
 ! N.B
 ! rang(ik) = # of symmetries | R_is ik = ik
 !
 use pars,           ONLY:SP
 use zeros,          ONLY:k_rlu_zero
 use vec_operate,    ONLY:rlu_v_is_zero,c2a,k2bz
 use D_lattice,      ONLY:nsym
 use R_lattice,      ONLY:rl_sop,bz_samp
 !
 type(bz_samp):: k
 !
 ! Work Space
 !
 integer         :: i1,i2,is
 real(SP)        :: v(3),kstar(nsym,3)
 logical         :: k_found
 !
 if (allocated(k%sstar)) return
 !
 YAMBO_ALLOC(k%star,(k%nibz,nsym))
 YAMBO_ALLOC(k%nstar,(k%nibz))
 YAMBO_ALLOC(k%weights,(k%nibz))
 !
 k%nstar=0
 k%star=0
 do i1=1,k%nibz
   do is=1,nsym
     v=matmul(rl_sop(:,:,is),k%pt(i1,:))
     call k2bz(v_in=v)
     call c2a(v_in=v,v_out=kstar(is,:),mode='ki2a')
     k_found=.false.
     do i2=1,k%nstar(i1)
       if (rlu_v_is_zero(kstar(is,:)-kstar(k%star(i1,i2),:),zero_=k_rlu_zero)) k_found=.true.
       if (k_found) exit
     enddo
     if (.not.k_found) k%nstar(i1)=k%nstar(i1)+1
     if (.not.k_found) k%star(i1,k%nstar(i1))=is
   enddo
 enddo
 !
 k%weights(:)=real(k%nstar(:))/real(sum(k%nstar))
 k%weights(:)=k%weights(:)/sum(k%weights)
 k%nbz=sum(k%nstar(:))
 !
 YAMBO_ALLOC(k%sstar,(k%nbz,2))
 i2=0
 do i1=1,k%nibz
   do is=1,k%nstar(i1)
     i2=i2+1
     k%sstar(i2,:)=(/i1,k%star(i1,is)/)
   enddo
 enddo
 !
end subroutine
!
!
subroutine k_ibz2bz(k,units,FORCE_BZ)
 !
 ! Output
 !
 !  k%ptbz
 !
 use pars,           ONLY:SP,schlen
 use vec_operate,    ONLY:c2a,k2bz
 use R_lattice,      ONLY:rl_sop,bz_samp
 !
 type(bz_samp)::k
 character(1) ::units
 logical      ::FORCE_BZ
 !
 ! Work Space  
 !
 real(SP):: k_bz(3)
 integer :: i1
 character(schlen)::ch
 !
 write (ch,'(2a)') trim(k%description),"-ptbz"
 !
 ! Clean
 !
 if (units=="d") then
   YAMBO_FREE(k%ptbz)
   k%units=' '
   return
 endif
 !
 if (allocated(k%ptbz).and.k%units==units) return
 !
 ! First the stars...
 !
 if (.not.allocated(k%sstar)) call k_expand(k) 
 !
 ! ...then the pts
 !
 YAMBO_ALLOC(k%ptbz,(k%nbz,3))
 !
 k%units=units
 !
 do i1=1,k%nbz
   !
   ! k_bz is in iku
   !
   k_bz=matmul(rl_sop(:,:,k%sstar(i1,2)),k%pt(k%sstar(i1,1),:)) 
   !
   ! Shift to the BZ
   !
   if (FORCE_BZ) call k2bz(k_bz)
   !
   if (units=='a') call c2a(v_in=k_bz,v_out=k%ptbz(i1,:),mode='ki2a')
   if (units=='i') k%ptbz(i1,:)=k_bz
   if (units=='c') call c2a(v_in=k_bz,v_out=k%ptbz(i1,:),mode='ki2c')
   !
 enddo
 !
end subroutine
