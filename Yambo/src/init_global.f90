!
! License-Identifier: GPL
!
! Copyright (C) 2006 The Yambo Team
!
! Authors (see AUTHORS file for details): AF DS
!
subroutine init_global(main_name)
 !
 use stderr,      ONLY:l_write_output
 use timing_m,    ONLY:timing_allocate,timing,nclockx,l_use_timing
 use openmp,      ONLY:OPENMP_initialize,OPENMP_update,&
                       n_threads,n_threads_X,master_thread,omp_is_off
 use parallel_m,  ONLY:CREATE_hosts_COMM,ncpu,myid,master_cpu
 !use cuda_m,       ONLY:cuda_setup,have_cuda
 !
 implicit none
 !
 character(*), intent(in) :: main_name
 
 !
 ! msg
 !
 if (l_write_output) then
   write(*,"(/,40('='))")
   write(*,"(5(' '),a)") trim(main_name)
   write(*,"(40('='),/)")
 endif
 !
 ! OpenMP
 !
#if defined _YOPENMP
 omp_is_off    = .FALSE.
 !if (index(INSTR,'noopenmp')>0) omp_is_off=.TRUE.
#else
 omp_is_off    = .TRUE.
#endif
 !
 call OPENMP_initialize( )
 call OPENMP_update(master_thread)
 !
 n_threads_X=n_threads

 !
 ! MPI & Node name
 ! AF: to be updated
 ncpu=1
 myid=0
 master_cpu=.true.
 call CREATE_hosts_COMM( )

 !
 ! CUDA environment defs
 !
 !call cuda_setup()

 !
 ! MPI
 !
 call PARALLEL_and_IO_Setup()

 !
 ! Clocks
 !
 if (l_use_timing) then
   call timing_allocate(nclockx)
   CALL timing(TRIM(main_name),OPR="start")
 endif

end subroutine init_global

