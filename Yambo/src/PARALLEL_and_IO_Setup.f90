!
! License-Identifier: GPL
!
! Copyright (C) 2013 The Yambo Team
!
! Authors (see AUTHORS file for details): AM
!
subroutine PARALLEL_and_IO_Setup()
 !
 use pars,           ONLY:lchlen,SP,DP
 use stderr,         ONLY:l_write_output
 use parallel_m,     ONLY:n_nodes,ncpu,PARALLEL_message,n_CPU_str_max,PAR_COM_WORLD,&
&                         mpi_comm_world,myid,COMM_reset,PAR_COM_NULL,host_name,&
&                         master_cpu
 use parallel_int,   ONLY:PP_bcast,PP_redux_wait 
 use openmp,         ONLY:n_threads_X,n_threads_SE,n_threads_RT,n_threads_DIP,n_threads_NL,n_threads
!use cuda_m,         ONLY:cuda_visible_devices,have_cuda_devices,cuda_gpu_subscription
 !
 implicit none
 !
 ! Work Space
 !
 integer           :: ID,i_err,i_s,n_max_threads,i_cpu,i_dev,ierr
 character(lchlen) :: dumb_ch
 !
!#ifdef _GPU
! integer, external :: cudaGetDevice
!#endif
 !
 !
 if (l_write_output) write(*,"(a)") 'MPI/OPENMP structure'
 !
 ! WORLD communicator setup
 !==========================
 !
 call COMM_reset(PAR_COM_WORLD)
 call COMM_reset(PAR_COM_NULL)
 !
#if defined _MPI
 !
 PAR_COM_WORLD%COMM  =mpi_comm_world
 PAR_COM_WORLD%CPU_id=myid
 PAR_COM_WORLD%n_CPU =ncpu
 !
 ! Nodes
 !==========================
 if (master_cpu) dumb_ch=host_name
 call PP_bcast(dumb_ch,0)
 do i_cpu=2,ncpu
   if (myid+1==i_cpu) then
     if (trim(host_name)==trim(dumb_ch)) n_nodes=0
     dumb_ch=host_name
   endif
   call PP_bcast(dumb_ch,i_cpu-1)
 enddo
 call PP_redux_wait(n_nodes)
 !
 ! GET ENVIROMENT definitions
 !============================
 !
 call PARALLEL_get_ENVIRONMENT_structure("ALL")
 !
#endif
 !
 ! CPU structure REPORT
 !======================
 !
 n_max_threads=maxval((/n_threads,n_threads_X/))
 !
 if (ncpu>1.or.n_max_threads>1) then
   !
   do i_s=0,n_CPU_str_max
     if (len_trim(PARALLEL_message(i_s))==0) cycle
     if (l_write_output) write( *,"(a)") 'MPI Tasks-Threads   '//trim(PARALLEL_message(i_s))
   enddo
   !
   if (l_write_output) write( *,"(a,i5)") 'MPI Tasks           ', ncpu
   !
 else
   !
   if (l_write_output) write( *,"(a,i5)") 'Cores               ', ncpu
   !
 endif
 !
 if (l_write_output) then
   write( *,"(a,i5)") 'Threads per Task    ',n_max_threads
   write( *,"(a,i5)") 'Nodes Computing     ',n_nodes(1)
 endif
 !
!#ifdef _GPU
! call msg('r', 'CUDA support        ',"yes")
! call msg('r', 'CUDA devices        ',have_cuda_devices)
! if (len_trim(cuda_visible_devices)>0) call msg('r', 'CUDA visible dev    ',trim(cuda_visible_devices))
! call msg('r', 'MPI tasks / GPU     ',cuda_gpu_subscription)
! ierr = cudaGetDevice(i_dev)
! call msg('s', 'MPI assigned to GPU ',i_dev)
!#endif
 !
 if (l_write_output) then
   if(SP/=DP) write( *,"(2a)") 'Precision           ','SINGLE'
   if(SP==DP) write( *,"(2a)") 'Precision           ','DOUBLE'
   !
   write(*,*)
 endif
 !
end subroutine
