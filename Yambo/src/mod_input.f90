
module input_m
  use pars
  implicit none

! integer, parameter :: nsym_x=48

  real(SP) :: ecutwfc
  real(SP) :: alat
  integer  :: nbnd_occ
  !
  integer  :: nbnd
  integer  :: nbnd_cmdline
  !
  integer  :: X_ng
  integer  :: X_ng_cmdline
  !
  integer  :: X_nq
  integer  :: X_nq_cmdline
  !
  integer  :: X_nfreq
  integer  :: X_nfreq_cmdline

end module input_m

