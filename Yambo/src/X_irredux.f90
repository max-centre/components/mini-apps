!
! License-Identifier: GPL
!
! Copyright (C) 2013 The Yambo Team
!
! Authors (see AUTHORS file for details): AMDSAFFA
!
#include<dev_defs.h>
#include<y_memory.h>
!
!> @callgraph
!> @callergraph
subroutine X_irredux(iq,what,X_par,Xen,Xk,Xw,X,Dip)
 !
 ! Non interacting Xo
 !
 ! OPENMP parallelism  (AF & FA)
 !   The current implementation is based on mutexes (locks).
 !   At the price of some replicated memory (Xo_res) it provides a much
 !   better omp scaling.
 !
 !   _NESTING introduces the nesting of scatterbamp parallel regions inside the
 !   outer region opened here (not working yet)
 !
 ! X terminator (BG kind) implemented (IM,AF)
 ! 
 use pars,          ONLY:SP,cZERO,schlen
 use stderr,        ONLY:l_write_output,debug_level
 use wrapper,       ONLY:V_plus_alpha_V,vv_caxpy
 use matrix,        ONLY:PAR_matrix
 use wave_func,     ONLY:WF
 use wave_func_int, ONLY:WF_load,WF_free
 use parallel_m,    ONLY:l_yminiapp_par,PAR_COM_X_WORLD,PAR_COM_RL_INDEX,myid,PAR_COM_X_WORLD_RL_resolved
 use parallel_int,  ONLY:PP_redux_wait
 use openmp,        ONLY:OPENMP_update,n_threads_X,master_thread,OPENMP_set_threads,n_threads_X,&
&                        n_out_threads,n_inn_threads,OPENMP_locks_reset,n_threads_FFT
#if defined _YOPENMP
 use openmp,        ONLY:OPENMP_compute_mutex,omp_locks
#endif
 use frequency,     ONLY:w_samp,bare_grid_N,coarse_grid_N,coarse_grid_Pt
 use electrons,     ONLY:levels
 use R_lattice,     ONLY:qindx_X,bz_samp,G_m_G,g_rot,DEV_VAR(g_rot)
 use D_lattice,     ONLY:i_space_inv
 use collision_el,  ONLY:elemental_collision,elemental_collision_free,elemental_collision_alloc
 use DIPOLES,       ONLY:DIPOLE_t,DIP_alloc
 use X_m_int,       ONLY:X_irredux_residuals
 use X_m,           ONLY:X_t,X_poles,X_Ein_poles,current_iq,X_poles_tab,X_lower_triangle_matrix_in_use,&
&                        self_detect_E_range,X_FILL_UP_matrix_only,use_X_DbGd,&
&                        X_par_lower_triangle
 use gpu_m,         ONLY:have_gpu
#ifdef _GPU
 use devxlib,       ONLY:devxlib_memcpy_d2d,devxlib_memcpy_d2h,devxlib_memcpy_h2d
#endif
 use timing_m,      ONLY:timing
 use input_m,       ONLY:X_ng
 !
 implicit none
 !
 type(PAR_matrix), target :: X_par
 type(levels)         :: Xen
 type(bz_samp)        :: Xk
 type(X_t)            :: X
 type(DIPOLE_t)       :: Dip
 type(w_samp)         :: Xw
 integer              :: iq
 character(*)         :: what
 !
 ! Work Space
 !
 integer              :: ig1,ig_row,ig_col,ig_row_transp,ig_col_transp,iw,n_poles,i_cg,i_bg,mutexid,ngrho,&
&                        X_cols_transp(2),X_rows1,X_rows2,X_cols1,X_cols2,X_rows1_l,X_rows2_l,X_nrows_l,is
 logical              :: skip_WF_load
 real(SP)             :: minmax_ehe(2,PAR_COM_X_WORLD%n_CPU),cutoff
 complex(SP)          :: GreenF(Xw%n_freqs)

 complex(SP),allocatable, target          :: Xo_res(:,:)
 complex(SP),allocatable, target DEV_ATTR :: Xo_res_d(:,:)
 complex(SP),allocatable DEV_ATTR :: work(:)
 complex(SP),pointer     DEV_ATTR :: Xo_res_p(:,:)
 complex(SP),pointer     DEV_ATTR :: X_par_p(:,:,:)
 complex(SP),pointer     DEV_ATTR :: X_par_lowtri_p(:,:,:)
 complex(SP)          :: GreenF_iw
 integer              :: lda,lwork,PAR_COM_RL_INDEX_n_CPU
 !
 integer, external    :: X_eh_setup
 type(elemental_collision), target :: Xo_scatt
 !
 !return
 if (l_write_output) write(*,*) "Starting X_irredux"
 !
 ! Defaults & Setups
 !===================
 GreenF                           = cZERO
 !
 X_rows1=X_par%rows(1)
 X_rows2=X_par%rows(2)
 X_cols1=X_par%cols(1)
 X_cols2=X_par%cols(2)
 PAR_COM_RL_INDEX_n_CPU=PAR_COM_RL_INDEX%n_CPU
 !
 if (PAR_COM_RL_INDEX_n_CPU> 1) lda=size(X_par_lower_triangle%blc(:,1,1))
 if (PAR_COM_RL_INDEX_n_CPU==1) lda=size(X_par%blc(:,1,1))
 !
 ! Logicals to use bare or Double Grid GF (no poles accumulation)
 !=======================================================
 !
 skip_WF_load= (iq==1.and.X%ng==1)
 !
 ! Dipoles
 !=========
 if (iq==1) call DIPOLE_dimensions(Xen,Dip,X%ib,X%q0)
 !
 ! WF load
 !=========
 ngrho=X%ng
 !
 !if(.not.skip_WF_load) call WF_load(WF,ngrho,maxval(qindx_X(:,:,2)),X%ib,(/1,Xk%nibz/))
 call WF_load(WF,ngrho,maxval(qindx_X(:,:,2)),X%ib,(/1,Xk%nibz/))
 !
 call timing(what//' (procedure)',OPR='start')
 !
 ! Poles tabulation
 !==================
 !
 if (iq/=current_iq) then
   !
   if (l_write_output) write(*,*) "Setting up X_eh poles (X_eh_setup)"
   ! 
   call FREQUENCIES_reset(Xw,"coarse_grid")
   !
   n_poles=X_eh_setup(-iq,X,Xen,Xk,minmax_ehe)
   !
   if (n_poles==0) write(*,*) ' [CPU has no poles]'
   !
   YAMBO_ALLOC(X_poles_tab,(n_poles,4))
   !
   call FREQUENCIES_coarse_grid('X',X_poles,n_poles,X%cg_percentual,X_Ein_poles,.false.)
   !
   minmax_ehe=0._SP
   !
   n_poles=X_eh_setup(iq,X,Xen,Xk,minmax_ehe(:,PAR_COM_X_WORLD%CPU_id+1))
   !
   YAMBO_FREE(X_poles)
   YAMBO_FREE(X_Ein_poles)
   !
   if (self_detect_E_range) then
     if (l_yminiapp_par) call PP_redux_wait(minmax_ehe,COMM=PAR_COM_X_WORLD%COMM)
     Xw%er(1)=minval(minmax_ehe(1,:))
     Xw%er(2)=maxval(minmax_ehe(2,:))
   endif
   !
   ! This call is needed as Xw%p is deallocated inside
   ! the q-loop of X_em1. But only when the EM1D is written or when it is not but we are not doing
   ! lifetimes calculations
   !
   call FREQUENCIES_setup(Xw)
   !
 endif
 !
 ! Time-Rev is Spatial Inv => only half X is eval
 !                            ===================
 if (X_FILL_UP_matrix_only.and.current_iq==0) write(*,*) '[X] Upper matrix triangle filled'
 !
 ! omp settings and workspace
 !=================================
 !
 if (l_write_output) then
   write(*,*) "setting up OMP part"
   write(*,*) "n_threads_X", n_threads_X
   write(*,*) 
 endif
 !
 if (have_gpu) then
   n_threads_FFT=1
   n_threads_X=1
   call OPENMP_set_threads(n_threads_in=n_threads_X, use_nested=.false.)
 else
#if defined _YOPENMP
#  if defined _NESTING
   call OPENMP_set_threads(n_threads_in=n_threads_X, use_nested=.true.)
   n_threads_FFT=n_inn_threads
   if (l_write_output) write(*,*) '[X] NESTED openmp parallelism on: n_out_threads/n_inn_threads = ',n_out_threads,n_inn_threads
#  else
   call OPENMP_set_threads(n_threads_in=n_threads_X, use_nested=.false.)
   n_threads_FFT=1
#  endif
   if(n_threads_X>1) call OPENMP_locks_reset(INIT=.true.,nlocks=16)
#endif
 endif

 ! workaround to avoid out-of-bouds of g_rot
 ! removed after introducing PARSER_close_ng
 !
 !write(*,*) "g_rot= ",allocated(g_rot),ngrho,size(g_rot,2),shape(g_rot)
 !write(*,*) "g_rot check ..."
 do is = 1, size(g_rot,2)
 do ig1 = 1, ngrho
    if ( g_rot(ig1,is) <= ngrho ) cycle
    write(*,*) "Wrong g_rot! Fixing it."
    g_rot(ig1,is)=mod(g_rot(ig1,is)-1,ngrho)+1
 enddo
 enddo
 !write(*,*) "done"
 !#ifdef _GPU
 !DEV_VAR(g_rot)=g_rot
 !#endif
 !
 X_par_p => DEV_VAR(X_par%blc)
 if (X_lower_triangle_matrix_in_use)  X_par_lowtri_p => DEV_VAR(X_par_lower_triangle%blc)

 !
 ! OpenMP setup
 !==============
 ! AF: note that DEV_OMP is switched off by the precompiler when _GPU is defined
 !
 !DEV_OMP parallel num_threads(n_out_threads) default(shared), &
 !DEV_OMP &        private(i_cg,GreenF,i_bg,Xo_res,Xo_res_p,Xo_scatt,ig_col,&
 !DEV_OMP &        ig1,ig_row_transp,iw,mutexid,work,lwork,&
 !DEV_OMP &        X_rows1_l,X_rows2_l,X_nrows_l,X_cols_transp)
 !
 call OPENMP_update(master_thread)
 !
 ! memory estimate and local alloc
 !=================================
 YAMBO_ALLOC(Xo_res,(X_par%rows(1):X_par%rows(2),X_par%cols(1):X_par%cols(2)))
#ifdef _GPU
 if (have_gpu) then
   YAMBO_ALLOC_GPU(DEV_VAR(Xo_res),(X_par%rows(1):X_par%rows(2),X_par%cols(1):X_par%cols(2)))
 endif
#endif
 !
 Xo_res_p => DEV_VAR(Xo_res)

 !
 lwork=ngrho
 !
#ifdef _GPU
 YAMBO_ALLOC_GPU(work,(lwork))
#else
 YAMBO_ALLOC(work,(lwork))
#endif
 !
 call elemental_collision_free(Xo_scatt)
 call elemental_collision_alloc(Xo_scatt,NG=ngrho,TITLE="Xo") 

 if (debug_level==1) write(*,*) "i_cg loop ",coarse_grid_N

#if defined _OMP_DYNAM_SCHEDULE
#  define Y_OMP_SCHEDULE schedule(dynamic)
#  else
#  define Y_OMP_SCHEDULE 
#endif

 !
 ! MAIN LOOP
 !===========
 !
 !DEV_OMP do Y_OMP_SCHEDULE
 !
 do i_cg = 1,coarse_grid_N
   !
   i_bg=sum(bare_grid_N(1:i_cg-1))+1
   !
   ! Drude term already accounted for
   !
   if( (iq==1) .and. abs(coarse_grid_Pt(i_cg))<1.E-5 ) goto 100
   !
   ! 1) First compute the residuals
   !================================
   if (debug_level==2) write(*,*) "X_irr res ",i_cg
   call X_irredux_residuals(Xen,Xk,X,Dip,i_cg,iq,Xo_res_p,Xo_scatt,work,lwork)
   !
   ! 2) Then the frequency dependent term
   !=======================================
   !
   if (debug_level==2) write(*,*) "X_GreenF ",i_cg
   call X_GreenF_analytical(iq,X_poles_tab(i_bg,:),Xw,Xen,Xk,GreenF,X%ordering,'G',.FALSE.,.FALSE.)
   !
   ! 3) Finally multiply residual and frequency dependent term
   !===========================================================
   if (debug_level==2) write(*,*) "freqs loop ",i_cg,Xw%n_freqs
   freq_loop:&
   do iw=1,Xw%n_freqs
     !
     ! ----    ----
     ! -xxx    -xxx
     ! ---- => ----
     ! ----    ----
     !
#ifdef _GPU
     !
     GreenF_iw=GreenF(iw)
     !
     !DEV_ACC data present(X_par_p,Xo_res_p)
     !DEV_ACC parallel loop 
     !DEV_CUF kernel do(2) <<<*,*>>>
     !DEV_OMPGPU target map(present,alloc:X_par_p,Xo_res_p)
     !DEV_OMPGPU teams loop collapse(2)
     do ig_col=X_cols1,X_cols2
       do ig1=X_rows1,X_rows2
         if (ig1 <= ig_col) X_par_p(ig1,ig_col,iw)=X_par_p(ig1,ig_col,iw)+GreenF_iw*Xo_res_p(ig1,ig_col)
       enddo
     enddo
     !DEV_OMPGPU end target
     !DEV_ACC end data
     !
#else
     !
     do ig_col=X_cols1,X_cols2
       !
#  if defined _YOPENMP
       if(n_threads_X>1) then
         call OPENMP_compute_mutex(ig_col,mutexid)
         call omp_set_lock(omp_locks(mutexid))
       endif
#  endif
       !
       X_rows1_l = X_rows1
       X_rows2_l = min(ig_col,X_rows2)
       X_nrows_l = X_rows2_l-X_rows1_l+1 
       !
       call V_plus_alpha_V(X_nrows_l,GreenF(iw),Xo_res(X_rows1_l:X_rows2_l,ig_col),X_par%blc(X_rows1_l:X_rows2_l,ig_col,iw))
       !
#  if defined _YOPENMP
       if(n_threads_X>1) call omp_unset_lock(omp_locks(mutexid))
#  endif
     enddo
     !
#endif
     !
     if (.not.X_FILL_UP_matrix_only) then
       !
       ! ----    ----
       ! -xxx    ----
       ! ---- => -x--
       ! ----    -x--
       !
#ifdef _GPU
       !
       if (PAR_COM_RL_INDEX_n_CPU>1) then
         !
         !DEV_ACC data present(X_par_lowtri_p,Xo_res_p)
         !DEV_ACC parallel loop collapse(2) 
         !DEV_CUF kernel do(2)
         !DEV_OMPGPU target map(present,alloc:X_par_lowtri_p,Xo_res_p)
         !DEV_OMPGPU teams loop collapse(2)
         do ig_col=X_cols1,X_cols2
         do ig1=X_rows1,X_rows2
           !
           if (ig1 < ig_col) then
             ig_row_transp = ig_col
             X_par_lowtri_p(ig_row_transp,ig1,iw)=X_par_lowtri_p(ig_row_transp,ig1,iw) + &
&                                                     GreenF_iw*conjg(Xo_res_p(ig1,ig_col))
           endif
           !
         enddo
         enddo
         !DEV_OMPGPU end target
         !DEV_ACC end data
         !
       else
         !
         !DEV_ACC data present(X_par_p,Xo_res_p)
         !DEV_ACC parallel loop collapse(2) 
         !DEV_CUF kernel do(2)
         !DEV_OMPGPU target map(present,alloc:X_par_p,Xo_res_p)
         !DEV_OMPGPU teams loop collapse(2)
         do ig_col=X_cols1,X_cols2
         do ig1=X_rows1,X_rows2
           !
           if (ig1 < ig_col) then
             ig_row_transp = ig_col
             X_par_p(ig_row_transp,ig1,iw)=X_par_p(ig_row_transp,ig1,iw)+GreenF_iw*conjg(Xo_res_p(ig1,ig_col))
           endif
           !
         enddo
         enddo
         !DEV_OMPGPU end target
         !DEV_ACC end data
         !
       endif
#else
       !
       do ig_col=X_cols1,X_cols2
         !
#  if defined _YOPENMP
         if(n_threads_X>1) then
           call OPENMP_compute_mutex(ig_col,mutexid)
           call omp_set_lock(omp_locks(mutexid))
         endif
#  endif
         !
         X_rows1_l = X_rows1
         X_rows2_l = min(ig_col-1,X_rows2)
         X_nrows_l = X_rows2_l-X_rows1_l+1
         !
         ig_row_transp = ig_col
         X_cols_transp = (/X_rows1_l,X_rows2_l/)
         !
         if (PAR_COM_RL_INDEX_n_CPU>1) then
           call vv_caxpy(X_nrows_l,GreenF(iw),conjg(Xo_res(X_rows1_l:X_rows2_l,ig_col)),1, & 
&                        X_par_lower_triangle%blc(ig_row_transp,X_cols_transp(1),iw),lda)
         else
           call vv_caxpy(X_nrows_l,GreenF(iw),conjg(Xo_res(X_rows1_l:X_rows2_l,ig_col)),1, &
&                        X_par%blc(ig_row_transp,X_cols_transp(1),iw),lda)
         endif
         !
#  if defined _YOPENMP
         if(n_threads_X>1) call omp_unset_lock(omp_locks(mutexid))
#  endif
         !
       enddo
#endif
       !
     endif
     !
   enddo freq_loop
   if (debug_level==2) write(*,*) "After freq loop ",i_cg,coarse_grid_N
   !
100 continue
   !
 enddo
 !
 !DEV_OMP end do
 if (debug_level==1) write(*,*) "After coarse grid loop" 
 !
 ! CLEAN
 !=======
#ifdef _GPU
 YAMBO_FREE_GPU(DEV_VAR(Xo_res))
#endif
 YAMBO_FREE(Xo_res)
#ifdef _GPU
 YAMBO_FREE_GPU(work)
#else
 YAMBO_FREE(work)
#endif
 YAMBO_FREE(work)
 if (debug_level==1) write(*,*) "colls free" 
 call elemental_collision_free(Xo_scatt)
 !
 !DEV_OMP end parallel
 !
 if (debug_level==1) write(*,*) "WF free" 
 call WF_free(WF)
 !call FREQUENCIES_reset(Xw,"coarse_grid")
 !YAMBO_FREE(X_poles_tab)
 !
 if (debug_level==1) write(*,*) "dips free" 
 if (iq==1) then
   call DIP_alloc('DIP_iR')
   call DIP_alloc('DIP_P')
   call DIP_alloc('DIP_v')
   call DIP_alloc('DIP_spin')
   call DIP_alloc('DIP_orbital')
   call DIP_alloc('P_square') 
   call DIP_alloc('DIP_P_spinor')
 endif
 !
 if (debug_level==1) write(*,*) "OMP end" 
 call OPENMP_update(master_thread) 
 call OPENMP_locks_reset()
 !
 !current_iq=iq
 current_iq=0
 !
 n_threads_FFT=0
 !
 call timing(what//' (procedure)',OPR='stop')
 if (debug_level==1) write(*,*) "timing info" 
 call timing(what//' (REDUX)',OPR='start')
 !
#ifdef _GPU
 if (have_gpu) then
   call devxlib_memcpy_d2h(X_par%blc,DEV_VAR(X_par%blc))
   if (X_lower_triangle_matrix_in_use) call devxlib_memcpy_d2h(X_par_lower_triangle%blc,DEV_VAR(X_par_lower_triangle%blc))
   YAMBO_FREE_GPU(DEV_VAR(X_par%blc))
   YAMBO_FREE_GPU(DEV_VAR(X_par_lower_triangle%blc))
 endif
#endif
 !
 if (l_yminiapp_par) then
 do iw=1,Xw%n_freqs
   call PP_redux_wait(X_par%blc(:,:,iw),COMM=PAR_COM_X_WORLD_RL_resolved%COMM)
   if (X_lower_triangle_matrix_in_use) call PP_redux_wait(X_par_lower_triangle%blc(:,:,iw),COMM=PAR_COM_X_WORLD_RL_resolved%COMM)
 enddo
 endif
 !
 call timing(what//' (REDUX)',OPR='stop')
 if (debug_level==1) write(*,*) "timing info2",.not.X_FILL_UP_matrix_only,PAR_COM_RL_INDEX%n_CPU==1 
 !
 ! Populate the lower triangle/piece of Xo 
 !=========================================
 !
 if (.not.X_FILL_UP_matrix_only.and.PAR_COM_RL_INDEX%n_CPU==1) goto 200
 !
 ! X_FILL_UP_matrix_only=.TRUE.
 !
 ! oooooo
 ! xooooo
 ! xxoooo
 ! ------
 ! ------
 ! ------
 !
 ! X_FILL_UP_matrix_only=.FALSE.
 !
 ! PAR_COM_RL_INDEX%n_CPU=1
 !
 ! oooooo
 ! oooooo
 ! oooooo
 ! oooooo
 ! oooooo
 ! oooooo
 !
 ! PAR_COM_RL_INDEX%n_CPU > 1
 !
 ! oooooo    xxx---
 ! xooooo    oxx---
 ! xxoooo    oox---
 ! ------ +  ooo---
 ! ------    ooo---
 ! ------    ooo---
 !
 ! "o" = calculated
 ! "x" = to be filled now
 !
 if (X_FILL_UP_matrix_only) then
   !
   !DEV_OMP parallel do default(shared), private(iw,ig_col,ig_row,ig_row_transp,ig_col_transp), collapse(2)
   do iw=1,Xw%n_freqs
   do ig_col=X_par%cols(1),X_par%cols(2)
     ig_row_transp=ig_col
     do ig_row=max(ig_col+1,X_par%rows(1)),X_par%rows(2)
       ig_col_transp=ig_row
       if (i_space_inv==1) X_par%blc(ig_row,ig_col,iw)=      X_par%blc(ig_row_transp,ig_col_transp,iw)
       if (i_space_inv==0) X_par%blc(ig_row,ig_col,iw)=conjg(X_par%blc(ig_row_transp,ig_col_transp,iw))
     enddo
   enddo
   enddo
   !DEV_OMP end parallel do
   !
 else if (PAR_COM_RL_INDEX%n_CPU>1) then
   !
   !DEV_OMP parallel do default(shared), private(iw,ig_col,ig_row,ig_row_transp,ig_col_transp), collapse(2)
   do iw=1,Xw%n_freqs
   do ig_col=X_par%cols(1),X_par%cols(2)
     ig_row_transp=ig_col
     do ig_row=max(ig_col,X_par%rows(1)),X_par%rows(2)
       ig_col_transp=ig_row
       ! FILL THE UPPER PART OF X_par_lower
       X_par_lower_triangle%blc(ig_row_transp,ig_col_transp,iw)=X_par%blc(ig_row_transp,ig_col_transp,iw)
       ! FILL THE LOWER PART OF X_par
       X_par%blc(ig_row,ig_col,iw)                             =X_par_lower_triangle%blc(ig_row,ig_col,iw)
     enddo
   enddo
   enddo
   !DEV_OMP end parallel do
   !
 endif
 !
200 if (debug_level==1) write(*,*) "Closing X_irredux"
 !
end subroutine
