!
! License-Identifier: GPL
!
! Copyright (C) 2006 The Yambo Team
!
! Authors (see AUTHORS file for details): AM DS
!
#include<y_memory.h>
!
subroutine X_GreenF_analytical(iq,transitions,Xw,Xen,Xk,GreenF,ordering,space,no_occupations,X_terminator)
 !
 ! Compute the GreenF in frequency space starting from it analytical
 ! expression as the Fourier Transform of a Lorentzian
 !
 ! In the K case, the anti-res part of Lo uses a w+\Omega (and not -\Omega) simply
 ! because this routine is always called using the i_res \Omega. Indeed 
 !
 !  \Omega_{anti-res} = -\Omega_{res}
 !
 use units
 use pars,       ONLY:SP,cZERO,cONE
 use X_m,        ONLY:use_X_DbGd,X_t,X_term_E,X_DbGd_nkpt_at_q,X_DbGd_nkpt_at_q_table
 use electrons,  ONLY:levels,spin_occ
 use R_lattice,  ONLY:qindx_X,bz_samp
 use frequency,  ONLY:w_samp
 use parallel_m, ONLY:PAR_Xk_bz_index
 !
 implicit none
 !
 integer      :: iq,transitions(4)
 type(w_samp) :: Xw
 type(levels) :: Xen
 type(bz_samp):: Xk
 complex(SP)  :: GreenF(Xw%n_freqs)
 character(*) :: ordering,space
 logical      :: no_occupations
 logical      :: X_terminator
 logical      :: intra_band
 !
 ! Work Space
 !
 integer      :: ikbz_ikpbz_FineGd_used,ikbz_FineGd,ikpbz_FineGd,iw,ikbz,ikpbz,iv,ic,i_spin,&
&                ik_FineGd,ikp_FineGd,ik,ikp,ikbz_mem
 real(SP)     :: ffac,cg_ffac,f_e,f_h,weight,W_,En1,En2
 complex(SP)  :: Z_,pole,pole_X_term
 !
 complex(SP),external      ::Lorentzian_FT
 !
 GreenF  = cZERO
 !
 ! Transitions
 !
 ikbz   = transitions(1)
 iv     = transitions(2)
 ic     = transitions(3)
 i_spin = transitions(4)
 ! 
 ikpbz  = qindx_X(iq,ikbz,1) 
 ik     = Xk%sstar(ikbz,1)
 ikp    = Xk%sstar(ikpbz,1)
 !
 W_=0._SP
 Z_=cONE
 if (allocated(Xen%W)) W_=abs(Xen%W(ic,ik,i_spin))+abs(Xen%W(iv,ikp,i_spin))
 if (allocated(Xen%Z)) Z_=Xen%Z(ic,ik,i_spin)*Xen%Z(iv,ikp,i_spin)
 !
 ! Case without DbGd
 !===================
 !
   !
   pole=cmplx(Xen%E(ic,ik,i_spin)-Xen%E(iv,ikp,i_spin),-W_,KIND=SP)
   !
   pole_X_term=0.0_SP
   if(X_terminator) pole_X_term= cmplx(X_term_E-Xen%E(iv,ikp,i_spin),-W_,KIND=SP)
   !
   ffac = 1._SP
   !
   do iw=1,Xw%n_freqs
     !
     GreenF(iw)= Lorentzian_FT(Xw%p(iw),pole,ordering)*ffac
     !
     if (X_terminator.and.ic>Xen%nbm(i_spin)) then
       GreenF(iw)= GreenF(iw) -Lorentzian_FT(Xw%p(iw),pole_X_term,ordering)*ffac  
     endif
     !
     if (X_terminator.and.ic<=Xen%nbm(i_spin)) then
       GreenF(iw)= -Lorentzian_FT(Xw%p(iw),pole_X_term,ordering)*ffac 
     endif
     !
   enddo
   !
   return
   !
 !
end subroutine
!
! License-Identifier: GPL
!
! Copyright (C) 2006 The Yambo Team
!
! Authors (see AUTHORS file for details): AMDS
!
function Lorentzian_FT(W,pole,ordering)
 !
 ! This subrutine returns the frequency dependent Green function
 ! which is the imaginary part of the
 ! Fourier transform of the Lorentzian function
 ! theta(t)*e^{-i*pole*t} [this is the retarded version, other ordering are computed]
 !
 ! AM,01/08/16. Note that aimag(pole) is always negative while aimag(W)>0
 !
 ! DS,31/10/18. Note that the present structure uses the relation e_{n,k}=e_{n,-k},
 !              See Fetter-Walecka, p.158 from Eq.(12.29) to (12.30)
 !              Such relation is always true. However it
 !              implies that f_{n,k}=f_{n,-k} and rho_{n,k}=rho_{n,-k} (eventually with a star)
 !              when the occupations and the oscillators are used.
 !              Here "n" is a generalized index for the transition when speaking about oscillators
 !              There are two cases where this is not true:
 !              a) with SOC rho_{n,k}/=rho_{n,-k} if SI is not a symmetry of the system
 !              b) in NEQ f_{n,k}=f_{n,-k} may not be true
 !              
 !
 use pars, ONLY:SP
 !
 implicit none
 !
 complex(SP)  ::pole,W,Lorentzian_FT
 character(*) ::ordering
 !              
 logical      ::ord_t_ordered,ord_true_t_or,ord_retarded_,ord_resonant_,ord_ares_tord,ord_ares_reta
 !
 ord_t_ordered= trim(ordering)=="T"      ! T-ordered
 ord_true_t_or= trim(ordering)=="t"      ! T-ordered with null damping !DALV: true time ordering for MPA
 ord_retarded_= trim(ordering)=="R"      ! Retarded 
 ord_resonant_= trim(ordering)=="r"      ! Resonant      part of the T-ordered/Retarded
 ord_ares_tord= trim(ordering)=="Ta"     ! Anti-resonant part of the T-ordered
 ord_ares_reta= trim(ordering)=="Ra"     ! Anti-resonant part of the Retarded
 !
 !                                  RESONANT       ANTIRESONANT
 !
 if ( ord_t_ordered ) Lorentzian_FT=1._SP/(W-pole)-1._SP/(conjg(W)+      pole )
 if ( ord_true_t_or ) Lorentzian_FT=1._SP/(W-pole)-1._SP/(      W +      pole ) 
 if ( ord_retarded_ ) Lorentzian_FT=1._SP/(W-pole)-1._SP/(      W +conjg(pole))       
 if ( ord_resonant_ ) Lorentzian_FT=1._SP/(W-pole)
 if ( ord_ares_tord ) Lorentzian_FT=              -1._SP/(conjg(W)+      pole )
 if ( ord_ares_reta ) Lorentzian_FT=              -1._SP/(      W +conjg(pole))
 !
end function
