
subroutine X_pre_setup(Xen,Xk,X,X_par)
 !
 use pars,          ONLY:SP
 use stderr,        ONLY:l_write_output,debug_level
 use X_m,           ONLY:X_t,X_use_lin_sys,X_use_gpu,Chi_linalg_mode,current_iq
 use fft_m,         ONLY:fft_dim,fft_multiplier
 use electrons,     ONLY:levels,n_sp_pol
 use wave_func,     ONLY:wf_ng
 use R_lattice,     ONLY:nqibz,bz_samp
 use parallel_m,    ONLY:PAR_COM_WORLD
 use stderr,        ONLY:intc
 use matrix,        ONLY:PAR_matrix
 use gpu_m,         ONLY:have_gpu
 use input_m,       ONLY:X_ng,X_nq
 !
 implicit none
 !
 type(X_t)    :: X 
 type(bz_samp):: Xk
 type(levels) :: Xen
 type(PAR_matrix) :: X_par
 ! 
 ! Work Space
 !
 !real(SP), external :: G2E
 !
 X%ng=X_ng
 Chi_linalg_mode="LIN_SYS"
 current_iq = 0
 !
 X%ib(1)=1
 X%ib(2)=Xen%nb
 !
 X%iq(1)=1
 if (X_nq > 0) then
   X%iq(2)=X_nq
 else
   X%iq(2)=nqibz
 endif
 !
 X%ordering="R"
 X%cg_percentual=100.0
 !
 X%ehe(1)=-1000._SP
 X%ehe(2)=+2000._SP
 !
 X_use_lin_sys=.true.
 X_use_gpu=have_gpu
 if (index(Chi_linalg_mode,"INVERSION")>0) X_use_lin_sys=.false.
 if (index(Chi_linalg_mode,"LIN_SYS")>0  ) X_use_lin_sys=.true.
 if (index(Chi_linalg_mode,"CPU")>0      ) X_use_gpu=.false.
 if (index(Chi_linalg_mode,"GPU")>0      ) X_use_gpu=.true..and.have_gpu
 !
 ! Copy of X dimension for Parallel default environment solver
 !
 X_par%rows=(/1,X%ng/)
 X_par%cols=(/1,X%ng/)
 !
 X%whoami=1
 !
 if (l_write_output) write(*,*) "defined X dimensions ",X_ng,wf_ng
 !
 fft_multiplier=1
 call fft_setup(X_ng,wf_ng,.false.)
 !
 if (l_write_output) write(*,*) "defined fft grid ",fft_dim
 !
#if defined _FFTW
 call fft_setup_plan_miniapp()
#endif
 !
end subroutine

