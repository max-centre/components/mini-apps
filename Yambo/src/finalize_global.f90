!
! License-Identifier: GPL
!
! Copyright (C) 2006 The Yambo Team
!
! Authors (see AUTHORS file for details): AM AF DS
!
subroutine finalize_global(main_name)
 !
 use timing_m,    ONLY:timing_deallocate,timing,global_list
 !
 implicit none
 !
 character(*), intent(in) :: main_name
 
 !
 ! Clocks
 !
 call timing(TRIM(main_name),OPR="stop")
 !
 write(*,*) "" 
 call TIMING_overview(global_list,trim(main_name))
 call timing_deallocate()

end subroutine finalize_global

