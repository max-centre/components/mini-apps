!
! License-Identifier: GPL
!
! Copyright (C) 2006 The Yambo Team
!
! Authors (see AUTHORS file for details): AM
!
#include<y_memory.h>
!
subroutine FREQUENCIES_coarse_grid(title,bg_pt,npts,cg_percentual,in_pt,lterm)
 !
 ! Input
 !-------
 ! integer     :: npts
 ! real(SP)    :: bg_pt(npts),cg_percentual
 ! real(SP)    :: in_pt(npts)   energies of the initial states (if lterm)
 ! logical     :: lterm         whether terminator techniques are used
 !
 ! Output
 !--------
 ! coarse_grid_N          ! Coarse grid points
 ! coarse_grid_Pt         ! Coarse grid point
 ! coarse_grid_index(ibg) ! Tells the index in the coarse
 !                        ! grid of the ibg-th element
 !                        ! of the original (not sorted) grid
 ! ordered_grid_index(ibg)! Tells the index in the reordered (not coarse) 
 !                        ! grid of the ibg-th element
 !                        ! of the original (not sorted) grid
 ! bare_grid_N(icg)       ! Tells how many poles are linked to the POLE of the
 !                        ! non degenerate new grid (icg)
 !
 use pars,        ONLY:SP,schlen
 use stderr,      ONLY:l_write_output
 use frequency,   ONLY:bare_grid_N,coarse_grid_N,coarse_grid_Pt,ordered_grid_index,coarse_grid_index
 use vec_operate, ONLY:sort
 !
 implicit none
 !
 character(*),intent(in) :: title
 integer,intent(in)      :: npts
 real(SP),intent(in)     :: bg_pt(npts),cg_percentual
 real(SP),intent(in)     :: in_pt(*)
 logical,intent(in)      :: lterm
 !
 ! DEFAULT TRESHOLD
 !
 real(SP), parameter :: default_treshold=1.E-5
 real(SP), parameter :: default_treshold_ini=1.E-5
 integer,  parameter :: MAX_cycles=1000
 !
 ! Work Space
 ! 
 integer ::i_cg,i_bg,i_bg_loc,i_bg_ref,dncg,ipos(npts,2),icycle,itresh
 real(SP)::tresh,tresh_ini,df
 logical ::lcycle
 character(schlen)   :: ch
 integer ,allocatable:: bg_sorted_x(:),i_vec(:)
 real(SP),allocatable:: bg_diffs(:),bg_sorted(:)
 real(SP),allocatable:: in_sorted(:)
 !
 if(npts==0) then
   YAMBO_ALLOC(ordered_grid_index,(npts))
   YAMBO_ALLOC(coarse_grid_index,(npts))
   YAMBO_ALLOC(bare_grid_N,(npts))
   YAMBO_ALLOC(coarse_grid_Pt,(npts))
   coarse_grid_N=0
   return
 endif   
 !
 if (cg_percentual==0._SP .or. npts==1) then
   !
   ! Zero cg_percentual does nothing!  This is used when 
   ! the response function is calculated using an external set of
   ! k-points/bands.
   !
   YAMBO_ALLOC(ordered_grid_index,(npts))
   YAMBO_ALLOC(coarse_grid_index,(npts))
   YAMBO_ALLOC(bare_grid_N,(npts))
   YAMBO_ALLOC(coarse_grid_Pt,(npts))
   coarse_grid_N=npts
   bare_grid_N=1
   coarse_grid_Pt=bg_pt
   forall ( i_bg=1:npts) ordered_grid_index(i_bg)=i_bg
   forall ( i_bg=1:npts) coarse_grid_index(i_bg)=i_bg
   return
 endif
 !
 YAMBO_ALLOC(bg_diffs,(npts-1))
 YAMBO_ALLOC(bg_sorted,(npts))
 YAMBO_ALLOC(bg_sorted_x,(npts))
 bg_sorted=bg_pt
 !
 YAMBO_ALLOC(in_sorted,(npts))
 in_sorted=0.0_SP
 !
 call sort(bg_sorted,indx=bg_sorted_x)
 do i_bg=1,npts-1
   bg_diffs(i_bg)=bg_sorted(i_bg+1)-bg_sorted(i_bg)
 enddo
 !
 if (lterm) then
   do i_bg=1,npts
     in_sorted(i_bg)=in_pt(bg_sorted_x(i_bg))
   enddo
 endif
 !
 if(npts>2) call sort(bg_diffs)
 !
 tresh=default_treshold
 tresh_ini=default_treshold_ini
 !
 if (cg_percentual<0.) tresh=minval(bg_diffs)+&
&                      abs(cg_percentual)/100._SP*(maxval(bg_diffs)-minval(bg_diffs))
 icycle=0
 coarse_grid_N=-1
 lcycle=.true.
 do while(lcycle)
   icycle=icycle+1
   i_cg=1
   i_bg_ref=1
   ipos(1,1)=i_cg
   ipos(bg_sorted_x(1),2)=1
   do i_bg=2,npts
     !
     ! df redefined using abs(), since the v-->v transition energies
     ! can be negative (when the terminator is used)
     !
     df=bg_sorted(i_bg)-bg_sorted(i_bg_ref)
     if (abs(df)>tresh .or. (lterm.and.abs(in_sorted(i_bg)-in_sorted(i_bg_ref))>tresh_ini) ) then
       i_cg=i_cg+1
       i_bg_ref=i_bg
     endif
     ipos(i_bg,1)=i_cg
     ipos(bg_sorted_x(i_bg),2)=i_bg
   enddo
   if (icycle==1) dncg=i_cg
   if (cg_percentual>0._SP) coarse_grid_N=max(int(cg_percentual*real(dncg,SP)/100._SP),1)
   if (i_cg<=coarse_grid_N.or.cg_percentual==100._SP.or.icycle>MAX_cycles) lcycle=.false.
   if (icycle==1) then
     itresh=npts-coarse_grid_N
     tresh=bg_diffs(max(itresh,1))
     cycle
   endif
   itresh=min(itresh+i_cg-coarse_grid_N,npts-1)
   tresh=bg_diffs(max(itresh,1))
 enddo
 coarse_grid_N=i_cg
 !
 YAMBO_ALLOC(ordered_grid_index,(npts))
 YAMBO_ALLOC(coarse_grid_index,(npts))
 YAMBO_ALLOC(bare_grid_N,(coarse_grid_N))
 YAMBO_ALLOC(coarse_grid_Pt,(coarse_grid_N))
 !
 ordered_grid_index=ipos(:,2)
 !
 i_cg=1
 coarse_grid_Pt=0._SP
 bare_grid_N=0
 do i_bg=1,npts
   if (ipos(i_bg,1)/=i_cg) then
     coarse_grid_Pt(i_cg)=coarse_grid_Pt(i_cg)/real(bare_grid_N(i_cg))
     i_cg=i_cg+1
   endif
   coarse_grid_Pt(i_cg)=coarse_grid_Pt(i_cg)+bg_sorted(i_bg)
   bare_grid_N(i_cg)=bare_grid_N(i_cg)+1
 enddo
 coarse_grid_Pt(coarse_grid_N)=coarse_grid_Pt(coarse_grid_N)/real(bare_grid_N(coarse_grid_N))
 !
 i_bg=0
 YAMBO_ALLOC(i_vec,(npts))
 do i_cg=1,coarse_grid_N
   do i_bg_loc=1,bare_grid_N(i_cg)
     i_bg=i_bg+1
     i_vec(i_bg)=i_cg
   enddo
 enddo
 forall(i_bg=1:npts) coarse_grid_index(i_bg)=i_vec( ordered_grid_index(i_bg) )
 YAMBO_FREE(i_vec)
 !
 if(.not.trim(title)=='COLL' .and. l_write_output) then
   write (ch,'(3a)') '[',title,'-CG] R(p) Tot o/o(of R)  '
   write(*,*) trim(ch),coarse_grid_N,npts,int(real(coarse_grid_N)/real(dncg)*100._SP)
 endif
 !
 YAMBO_FREE(bg_diffs)
 YAMBO_FREE(bg_sorted)
 YAMBO_FREE(bg_sorted_x)
 YAMBO_FREE(in_sorted)
 !
end subroutine
!
! License-Identifier: GPL
!
! Copyright (C) 2006 The Yambo Team
!
! Authors (see AUTHORS file for details): AM DS
!
subroutine FREQUENCIES_setup(W)
 !
 ! %er %dr %n_freqs -> %p
 !
 use pars,          ONLY:SP,cI
 use stderr,        ONLY:l_write_output
 use frequency,     ONLY:w_samp
 !
 implicit none
 !
 type(w_samp):: W 
 ! 
 ! Work Space
 !
 integer          :: i1
 logical          :: metallic_damping
 real(SP)         :: delta_w,w_n
 complex(SP)      :: w_tmp,w_tmp2(2)
 real(SP),external:: FREQUENCIES_damping
 !
 if (l_write_output) write(*,*) "Setting up frequencies"
 !
 !If %p is associated and consitent with %er/%dr return
 !
 if (allocated(W%p).and.trim(W%grid_type)=="ra") then
   if (size(W%p)==W%n_freqs.and.&
&      abs(  real(W%p(1))-W%er(1) )<1.E-5.and.&
&      abs( aimag(W%p(1))-W%dr(1) )<1.E-5.and.&
&      abs(  real(W%p(W%n_freqs))-W%er(2) )<1.E-5.and.&
&      abs( aimag(W%p(W%n_freqs))-W%dr(2) )<1.E-5) return
 endif
 !
 if (allocated(W%p)) then
 YAMBO_FREE(W%p)
 endif

 !
 ! Regular grids
 ! First the total/mem steps
 !
 if (W%er(2)==W%er(1)) W%n_freqs=1
 W%n_freqs=nint(W%per_memstps*real(W%n_freqs)/100._SP)
 W%n_freqs=max(1, W%n_freqs)
 !
 YAMBO_ALLOC(W%p,(W%n_freqs))
 !
 W%p(1)=cmplx(W%er(1),W%dr(1),SP)
 if (W%n_freqs==1) then
   W%er(2)=W%er(1)
   W%dr(2)=W%dr(1)
   return
 endif
 !
 delta_w=(W%er(2)-W%er(1))/(W%n_freqs-1)
 !
 forall(i1=1:W%n_freqs-1) W%p(i1+1)=cmplx(W%er(1)+delta_w*real(i1,SP),0.,SP)
 !
 do i1=1,W%n_freqs
   W%p(i1)=real(W%p(i1))+FREQUENCIES_damping(W,real(W%p(i1)))*cI
   if(abs(W%p(i1))==0._SP) W%p(i1)=W%p(i1)+1.E-10_SP
 enddo
 !
end subroutine
!
! License-Identifier: GPL
!
! Copyright (C) 2006 The Yambo Team
!
! Authors (see AUTHORS file for details): AM
!
function FREQUENCIES_damping(W,E)
 !
 use pars,      ONLY:SP
 use frequency, ONLY:w_samp
 implicit none
 !
 real(SP)      :: FREQUENCIES_damping,E,beta
 type(w_samp)  :: W
 ! 
 ! Work Space
 !
 real(SP):: e1,e2,d1,d2,E_
 !
 e1=W%er(1)
 e2=W%er(2)
 d1=W%dr(1)
 d2=W%dr(2)
 E_=E
 !
 if (W%er(1)<0._SP .and. W%er(2)<=0._SP) then
   e1=-W%er(2)
   e2=-W%er(1)
   E_=-E
 endif
 !
 if (W%er(1)<0._SP .and. W%er(2)>0._SP ) then
   !
   ! Cannot use Fermi damping is the E range is not everywhere positve
   !
   W%damp_reference=0._SP
   if (E>=0._SP) then
     e1=0._SP
     e2=W%er(2)
   else
     e1=0._SP
     e2=-W%er(1)
     E_=-E
   endif
 endif
 !
 ! To use Fermi we need that d2/2 > d1 
 !
 if (d2/2._SP<= d1) then
   W%damp_reference=0._SP
 endif
 !
 if (W%damp_reference==0._SP) then
   !
   !           /(E2,D2)
   !          /
   !         /
   !   (E1,D1) 
   !
   FREQUENCIES_damping=d1+(d2-d1)/(e2-e1)*(E_-e1)
 else
   beta=1._SP/(W%damp_reference-e1)*log( (d2-d1)/d1 )
   FREQUENCIES_damping=d2/(1._SP+exp(-beta*(E_-W%damp_reference)))
 endif
 !
end function

!
! License-Identifier: GPL
!
! Copyright (C) 2006 The Yambo Team
!
! Authors (see AUTHORS file for details): AM
!
subroutine FREQUENCIES_reset(Xw,what)
 !
 ! Input
 !-------
 ! integer     :: npts
 ! real(SP)    :: bg_pt(npts),cg_percentual
 !
 ! Deallocate:  
 ! bg_pt(:), cg_pt(:), cg_index_bg(:),
 ! X_poles_tab(:), rg_index_bg(:), bg_npts(:)
 !
 use frequency,   ONLY:w_samp,bare_grid_N,coarse_grid_Pt,ordered_grid_index,coarse_grid_index
 use X_m,         ONLY:X_poles_tab
#include<y_memory.h>
 !
 type(w_samp)  :: Xw
 character(*)  :: what
 !
 if (what=="points".or.what=="all") then
   YAMBO_FREE(Xw%p)
 endif
 !
 if (what=="coarse_grid".or.what=="all") then
   YAMBO_FREE(ordered_grid_index)
   if (allocated(X_poles_tab)) then
     YAMBO_FREE(X_poles_tab)
     YAMBO_FREE(bare_grid_N)
     YAMBO_FREE(coarse_grid_Pt)
     YAMBO_FREE(coarse_grid_index)
   endif
 endif
 !
end subroutine
