program test_charge_density
  !-----------------------------------------------------------------------
  !
  !
  use iso_fortran_env, only : dp => real64
  use fft_types, only: fft_type_descriptor 
  use fft_interfaces, only: invfft, fwfft
  use fft_ggen, only : fft_set_nl
  use interface_cross, only: cross 
  !
  !... para libraries
  !
  use mpi
  use mp,         only : mp_size, mp_rank, mp_bcast, mp_sum, mp_start, mp_barrier
  !
  implicit none
  !
! global input variables read from file:
  integer                   :: nelec,ncenters 
  real(dp)                  :: gcutm 
  ! 
  !global variables 
  logical                   :: iope, lpara
  integer                   :: npes, mype, comm, root
  real(dp),allocatable      :: at(:,:), bg(:,:) 
  complex(dp),allocatable   :: rhog(:), psic(:) 
  real(dp), allocatable     :: g(:,:)   
  integer                   :: ngm, ngm_g 
  integer,allocatable       :: mill (:,:)
  logical                   :: gamma_only 
  type(fft_type_descriptor) :: dfftp
  integer                   :: mpistat  
  !work-variables           :: 
  !
  !
  !
  allocate(at(3,3), bg(3,3)) 
  call initialize_mpi() 
  !
  call read_input_and_check() 
  call initialize_fftxlib() 
  call initialize_miller_indices()
  call allocate_gvectors()  
  call generate_charge() 
  call set_nl() 
  call deallocate_gvectors() 
  call perform_inv_fft() 
  call perform_fw_fft() 
  call compare_temp_with_starting() 
#if defined(__MPI) 
  call mpi_finalize(mpistat)
#endif
 !
contains 
  subroutine initialize_mpi() 
    implicit none 
    integer :: ierr
#if defined(__MPI)
    call mpi_init(ierr)
    comm = mpi_comm_world
    call mp_start(npes, mype, comm) 
    root = 0
    if (mype == root) then
      iope = .true.
    else
      iope = .false.
    endif
    lpara=.true. 
#else
    mype = 0
    npes = 1
    comm = 0
    root = 0
    iope = .true.
    lpara=.false.
#endif
  end subroutine initialize_mpi
  !
  subroutine read_input_and_check() 
    implicit none
    real(dp) :: a1(3), a2(3), a3(3)
    namelist /input/  a1, a2, a3, gcutm, nelec, ncenters
    real(dp)  :: vol     
    real(dp),parameter :: tpi = 8._dp * atan(1._dp) 
    a1 = 0.  
    a2 = 0. 
    a3 = 0. 
    gcutm = 25 
    nelec = 1 
    gamma_only = .false. 
    ncenters = 4
    if( iope ) then 
      read(5,input)
      at(:,1) = a1
      at(:,2) = a2
      at(:,3) = a3
    end if 
    if (lpara) then 
       call mp_bcast(at,0,comm) 
       call mp_bcast(nelec,0,comm) 
       call mp_bcast(gcutm,0, comm)
       call mp_bcast(gamma_only,0,comm)  
    end if 
    vol = dot_product(at(:,1), cross(at(:,2), at(:,3)))  
    if (  vol .lt. 1.d-3 ) then 
      print '("please provide in input the lattice vectors a1, a2, a3 with the right chirality")'
      stop
    end if 
    bg(:,1) = tpi / vol * cross(at(:,2), at(:,3)) 
    bg(:,2) = tpi / vol * cross(at(:,3), at(:,1)) 
    bg(:,3) = tpi / vol * cross(at(:,1), at(:,2)) 
  end subroutine read_input_and_check 
  !  
  subroutine allocate_gvectors()
    implicit none
    integer :: ng  
    allocate(g(3,ngm))

    do ng = 1,ngm 
       g(:,ng) = mill(1,ng) * bg (:,1) + mill(2, ng) * bg(:,2) + mill(3,ng) * bg(:,3) 
    end do  
  end subroutine allocate_gvectors
  ! 
  subroutine deallocate_gvectors()
    implicit none
    deallocate(g) 
  end subroutine deallocate_gvectors 
  !
  subroutine initialize_fftxlib()
    use fft_types, only: fft_type_init 
    use stick_base, only: sticks_map 
    implicit none 
    type(sticks_map) :: smap
    type(fft_type_descriptor) :: dfftpp
    !
    call fft_type_init(dfftp, smap, "rho",  gamma_only, lpara, comm, at, bg, gcutm, 4.d0, nyfft=1, nmany=1) 
    dfftp%rho_clock_label='fftp'
  end subroutine initialize_fftxlib 
  ! 
  subroutine initialize_miller_indices()
    use interface_gcreate, only: gcreate 
    implicit none 
    integer,allocatable   :: ig_l2g(:)
    !
    call gcreate(dfftp, bg, gcutm, ig_l2g, mill)
    deallocate(ig_l2g) 
    ngm = dfftp%ngl(mype+1)  
    ngm_g = ngm 
    if (lpara) then 
      call mp_sum(ngm_g,comm) 
    end if  
    
  end subroutine initialize_miller_indices 
  ! 
  subroutine generate_charge() 
    implicit none
    real(dp),parameter  :: pi = 4._dp * atan(1._dp) 
    real(dp),allocatable  :: centers(:,:,:), sigmas(:,:)
    real(dp)              :: gg 
    complex(dp)           :: argstr
    integer               :: ig, iel, ice
    !
    allocate (rhog(ngm)) 
    allocate(centers(3,nelec, ncenters), sigmas(nelec,ncenters)) 
    call random_number(centers) 
    call random_number(sigmas) 
    centers = 0.5_dp * centers
    sigmas = max(abs(sigmas), 0.2) 
    rhog = cmplx(0._dp, 0._dp, kind=dp) 
    do ig = 1, ngm 
       gg = dot_product(g(:,ig),g(:,ig)) 
       do iel = 1, nelec 
          do ice = 1, ncenters 
            argstr = cmplx(0._dp, 1._dp) * dot_product(g(:,ig),centers(1, iel, ice)*at(:,1)+&
                                                               centers(2, iel, ice)*at(:,2)+&
                                                               centers(3, iel, ice)*at(:,3))  
            rhog(ig) = rhog(ig) + exp(-gg * sigmas(iel,ice)**2 / 4._dp ) * exp(argstr) 
          end do 
       end do 
    end do
    if (dot_product(g(:,1),g(:,1)) == 0._dp) print '("total charge =",F16.8)', rhog(1)%re 
  end subroutine generate_charge   
  ! 
  subroutine set_nl() 
    use fft_ggen, only: fft_set_nl 
    implicit none
    integer :: ig
    real(dp), parameter :: tpi = 8._dp * atan(1._dp)  
    g = g/tpi
    call fft_set_nl( dfftp, at, g)
    !check everything is fine with the grid we created 
    print *, pack([(ig, ig=1,dfftp%ngm)], dfftp%nl <= 0) 
  end subroutine set_nl 
  ! 
 !
 subroutine perform_inv_fft() 
   use fft_helper_subroutines, only: fftx_oned2threed 
   use fft_interfaces, only: invfft 
   implicit none 
   complex(dp),allocatable  :: psic_temp(:) 
   allocate(psic(dfftp%nnr)) 
   psic = (0._dp, 0._dp) 
   call fftx_oned2threed(dfftp, psic, rhog) 
   call invfft("Rho", psic, dfftp) 
 end subroutine perform_inv_fft 
 ! 
 subroutine perform_fw_fft()
   use fft_interfaces, only: fwfft 
   implicit none 
   call fwfft("Rho", psic, dfftp) 
 end subroutine perform_fw_fft 
 !
 subroutine compare_temp_with_starting() 
   use fft_helper_subroutines, only: fftx_threed2oned, fftx_oned2threed
   implicit none
   complex(dp),allocatable  :: rhog_temp(:) 
   integer                  :: ig 
   complex(dp)              :: res 
   allocate(rhog_temp, mold=rhog) 
   rhog_temp = cmplx(0._dp, 0._dp, kind = dp) 
   call fftx_threed2oned(dfftp, psic, rhog_temp) 
   rhog_temp = rhog_temp - rhog 
   res = cmplx(0._dp, 0._dp, kind =dp) + sum(rhog_temp) 
   call mp_sum(res, comm) 
   if (iope)  print '("check residual =",E16.8)', res%re**2 + res%im**2  
end subroutine compare_temp_with_starting 
!
end program test_charge_density
!
