module interface_gcreate
  use iso_fortran_env, only: dp=>real64
  use fft_types, only: fft_type_descriptor 
  implicit none 
  private
  public   gcreate
  interface
    subroutine gcreate(desc, bg, gcut, ig_l2g, mill) 
      import  dp, fft_type_descriptor 
      implicit none
      type(fft_type_descriptor),intent(in)  :: desc 
      real(dp),intent(in)                   :: bg(3,3) 
      real(dp),intent(in)                   :: gcut 
      integer,allocatable,intent(inout)     :: ig_l2g(:), mill(:,:) 
    end subroutine gcreate
  end interface
end module interface_gcreate   

subroutine gcreate(desc, bg, gcut, ig_l2g, mill )
  use iso_fortran_env,only: dp => real64
  use fft_types, only: fft_type_descriptor, fft_stick_index
  use mp, only: mp_sum
  use stick_base, only: hpsort
  use interface_cross, only: cross
  implicit none
  type(fft_type_descriptor),intent(in) :: desc 
  real(dp),intent(in)                 ::  bg(3,3)
  real(dp),intent(in)                  :: gcut 
  integer,allocatable,intent(inout)    :: ig_l2g(:) 
  integer,allocatable,intent(out)      :: mill(:,:)
  ! 
  integer                              :: nx, ny, nz, ix, iy, iz  
  integer,allocatable                  :: mill_unsorted(:,:),  g2l(:),&
                                          igsrt(:), ig2iz_0(:)  
  integer                              :: ngm_g, ngm, ng, ng0, ngstart, nglocal, nglocal_start,& 
                                          chunk, ng_loc 
  real(dp)                             :: t(3), t0(3) 
  real(dp),allocatable                 :: tt(:),g(:), g2sort_g(:) 
  logical                              :: gamma_only, is_local
  !
  allocate(tt(desc%nr3))
  nx = (desc%nr1x - 1)/2 
  ny = (desc%nr2x - 1)/2 
  nz = (desc%nr3x - 1)/2 
  gamma_only = desc%lgamma
  ngm_g = desc%ngl(desc%mype+1)  
  call mp_sum(ngm_g, desc%comm)
  if (gamma_only) then 
    if (desc%mype == 0) print '("Gamma case not implemented" )'
    return   
  end if 
   
  allocate(ig2iz_0(2*nz+1), g2sort_g(ngm_g), g2l(ngm_g))
  allocate(mill_unsorted(3,ngm_g)) 
  ngm = 0  
  nglocal = 0 
  do ix = -nx, nx
    do iy = -ny, nz
      is_local = (.not. desc%lpara .or. fft_stick_index(desc, ix, iy ) /= 0 )
      t0 = ix * bg(:,1) + iy * bg(:,2)
      ig2iz_0 = 0 
      ng0=0
      do iz = -nz, nz 
        t = t0 + iz * bg(:,3)
        tt(iz+nz+1) = t(1)**2 + t(2)**2 + t(3)**2
        if ((tt(iz+nz+1)) .lt. 1.e-8_dp  ) tt(iz+nz+1) = 0.d0  
        if (tt(iz+nz+1) .lt. gcut) then 
          ng0 = ng0 + 1 
          ig2iz_0(ng0) = iz  
        end if 
      end do
      !atomic capture and update  
      ngstart = ngm  
      ngm = ngm + ng0  
      !end atomic 
      if (is_local) then 
        !atomic capture and update 
        nglocal_start = nglocal 
        nglocal = nglocal + ng0
        !end atomic
      end if 
      if (ngm .gt. ngm_g ) then 
         print *, ngm, ngm_g 
         call errore ('create new grid 1', 'too many vectors',1 )
      end if
      do ng = 1, ng0  
        g2sort_g(ng + ngstart) = tt(ig2iz_0(ng)+nz+1)
        if (is_local) then
          mill_unsorted(:,nglocal_start + ng ) = [ix,iy,ig2iz_0(ng)] 
          g2l (ngstart + ng ) = nglocal_start + ng
        else 
          g2l(ngstart + ng) = 0 
        end if 
      end do
    end do     
  end do 
  if (ngm /= ngm_g) call errore ('create new grid 2', 'too few vectors found',1)
  allocate(igsrt(ngm)) 
  igsrt(1) = 0  
  call hpsort (ngm_g, g2sort_g, igsrt ) 
  deallocate(tt,g2sort_g) 
  allocate (mill(3, nglocal),ig_l2g(nglocal))  
  ng_loc = 0 
  do ng = 1, ngm_g 
    if (g2l(igsrt(ng)) /= 0 ) then 
       ng_loc = ng_loc + 1  
       ig_l2g(ng_loc)  =  ng  
       mill(:, ng_loc) =  mill_unsorted(:, g2l(igsrt(ng))) 
    end if
  end do 
  if (ng_loc <  nglocal) call errore('too few miller indices','ciao',1) 
  if (ng_loc > nglocal) call errore ('too many miller indices', 'ciao', 1)  
  deallocate(mill_unsorted,igsrt,g2l)   
end subroutine gcreate 

