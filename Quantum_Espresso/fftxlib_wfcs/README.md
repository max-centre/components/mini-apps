# FFTXlib_wave 


## Origin
Quantum ESPRESSO (FFTXlib) 


## Short description
Generates many 3D data sets in G space, distributed in the Wave function grid of QE, and transforms all 
of them back and forth performing 3D FFTs (in wave modality). 
Reproduces the operation of FFTXlib for many wave functions and allows to explore 
trade-off between R&G, band distribution, and the speedup for the execution with many functions modality inside QE with many ffts in batched mode. 

## Build instructions

compile FFTXlib see git@gitlab.com:max-centre/components/fftxlib7.git  
compile qeMPlib see git@gitlab.com:max-centre/components/qemplib.git

Update the Makefile with the roots of the two directories, the name of the MPI compiler and execute  
`make` 


## Run instructions

To run the  program do: 
`mpirun -np 4 ./miniapp2 < input` 

## Test cases description
You may modify sizes of the simulation editing the input, increasing the length of the 3 a vector, 
and the the gcutw  values. These factors increase the dimension of the FFT 3D grid and number of distributed 
plane waves. The number of distributed plane waves is printed out when running. 
The number of electrons per center and  the number of centers determine the number of processed bands which 
is equal to ncenter times electrons.  The collective cost of the calculation should in total scale as 
npw as nbands. 

