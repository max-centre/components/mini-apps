module  interface_cross
  use iso_fortran_env, only: dp=>real64
  implicit none 
  private
  public  cross
  interface
    function cross(u,v) result(w) 
      import dp 
      implicit none
      real(dp) :: w(3) 
      real(dp),intent(in) :: u(3), v(3) 
    end function cross
  end interface
end module interface_cross  
         
function cross(u,v) result (w) 
  use iso_fortran_env, only: dp=> real64 
  implicit none
  real(dp)            :: w(3)
  real(dp),intent(in) :: u(3),v(3)
  !
  integer             :: i,e(3)
  do i = 0, 2
    e  = cycl3(i, [1,2,3]) 
    w(e(1)) = u(e(2)) * v(e(3)) - u(e(3)) * v(e(2)) 
  end do 
contains     
  function cycl3(k,seed) result (p)
    implicit none 
    integer :: p(3)
    integer,intent(in) :: k, seed(3)
    integer  :: r
    p = [ (seed(mod(r+k,3)+1),r=0,2)] 
  end function cycl3  
end function cross   
