program test_wfcs
  !-----------------------------------------------------------------------
  !
  !
  use iso_fortran_env, only : dp => real64
  use fft_types, only: fft_type_descriptor 
  use fft_interfaces, only: invfft, fwfft
  use fft_ggen, only : fft_set_nl
  use interface_cross, only: cross 
  use fft_wave, only: wave_g2r, wave_r2g 
  !
  !... para libraries
  !
  use mpi
  use mp,         only : mp_size, mp_rank, mp_bcast, mp_sum, mp_start, mp_barrier
  use mp_wave,    only : splitwf, mergekg, mergewf
  !
  implicit none
  !
! global input variables read from file:
  integer                   :: nelec,ncenters, nmany 
  real(dp)                  :: gcutw 
  ! 
  !global variables 
  logical                   :: iope, lpara
  integer                   :: npes, mype, comm, root
  integer,allocatable       :: igk_dum(:) 
  real(dp),allocatable      :: at(:,:), bg(:,:) 
  complex(dp),allocatable   :: wfcs(:,:), wfcs_temp(:,:), psic(:) 
  real(dp), allocatable     :: g(:,:)   
  integer                   :: ngm, ngm_g, npw  
  integer,allocatable       :: mill (:,:)
  logical                   :: gamma_only 
  type(fft_type_descriptor) :: dfftp, dffts
  integer                   :: mpistat  
  complex(dp)               :: res 
  !work-variables           :: 
  !
  integer                   :: ig, iw
  !
  !
  allocate(at(3,3),bg(3,3)) 
  call initialize_mpi() 
  !
  call read_input_and_check() 
  call initialize_fftxlib() 
  call initialize_miller_indices()
  call allocate_gvectors()  
  igk_dum = [(ig, ig =1, npw)] 
  call generate_wavefunctions() 
  call set_nl() 
  call deallocate_gvectors() 
  allocate(wfcs_temp(npw,1)) 
  res = cmplx(0._dp, 0._dp, KIND=dp) 
  allocate(psic(dfftp%nnr)) 
  !$omp teams distribute 
  !$omp do 
  do iw = 1, ncenters * nelec 
    call wave_g2r (wfcs(1:npw,iw:iw),psic, dffts, igk = igk_dum) 
    call wave_r2g (psic, wfcs_temp(1:npw,:), dffts, igk = igk_dum)
    call compare_temp_with_starting(wfcs(:,iw), wfcs_temp(:,1),res) 
  end do 
  !$omp end do
  call mp_sum(res, comm)
  if (mype == 0) print *, "total difference check", sqrt(res%re**2 + res%im**2)   
  call mpi_finalize(mpistat)
 !
contains 
  subroutine initialize_mpi() 
    implicit none 
    integer :: ierr
#if defined(__MPI)
    call mpi_init(ierr)
    comm = mpi_comm_world
    call mp_start(npes, mype, comm) 
    root = 0
    if (mype == root) then
      iope = .true.
    else
      iope = .false.
    endif
    lpara=.true. 
#else
    mype = 0
    npes = 1
    comm = 0
    ntgs = 1
    root = 0
    iope = .true.
    lpara=.false.
#endif
  end subroutine initialize_mpi
  !
  subroutine read_input_and_check() 
    implicit none
    real(dp) :: a1(3), a2(3), a3(3)
    namelist /input/  a1, a2, a3, gcutw, nelec, ncenters, nmany
    real(dp)  :: vol     
    real(dp),parameter :: tpi = 8._dp * atan(1._dp) 
    a1 = 0.  
    a2 = 0. 
    a3 = 0. 
    gcutw = 0 
    nelec = 0 
    gamma_only = .false. 
    ncenters = 0
    if( iope ) then 
      read(5,input)
      at(:,1) = a1
      at(:,2) = a2
      at(:,3) = a3
    end if 
    if (lpara) then 
       call mp_bcast(at,0,comm) 
       call mp_bcast(nelec,0,comm) 
       call mp_bcast(ncenters,0,comm) 
       call mp_bcast(gcutw,0, comm)
       call mp_bcast(gamma_only,0,comm)  
    end if 
    vol = dot_product(at(:,1), cross(at(:,2), at(:,3)))  
    if (  vol .lt. 1.d-3 ) then 
      print '("please provide in input the lattice vectors a1, a2, a3 with the right chirality")'
      stop
    end if 
    bg(:,1) = tpi / vol * cross(at(:,2), at(:,3)) 
    bg(:,2) = tpi / vol * cross(at(:,3), at(:,1)) 
    bg(:,3) = tpi / vol * cross(at(:,1), at(:,2)) 
  end subroutine read_input_and_check 
  !  
  subroutine allocate_gvectors()
    implicit none
    integer :: ng  
    allocate(g(3,ngm))
    do ng = 1,ngm 
       g(:,ng) = mill(1,ng) * bg (:,1) + mill(2, ng) * bg(:,2) + mill(3,ng) * bg(:,3) 
    end do  
  end subroutine allocate_gvectors
  ! 
  subroutine deallocate_gvectors()
    implicit none
    deallocate(g) 
  end subroutine deallocate_gvectors 
  !
  subroutine initialize_fftxlib()
    use fft_types, only: fft_type_init 
    use stick_base, only: sticks_map 
    implicit none 
    type(sticks_map) :: smap
    !
    call fft_type_init(dfftp, smap, 'wave', gamma_only, lpara, comm, at, bg, gcutw, 4.d0, nyfft=1, nmany=1) 
    call fft_type_init(dffts, smap, "rho",  gamma_only, lpara, comm, at, bg, 4._dp * gcutw, 4.d0, nyfft=1, nmany=1) 
    dfftp%rho_clock_label='fftp'
    dffts%wave_clock_label = 'ffts' 
  end subroutine initialize_fftxlib 
  ! 
  subroutine initialize_miller_indices()
    use interface_gcreate, only: gcreate 
    implicit none 
    integer,allocatable   :: ig_l2g(:)
    !
    call gcreate(dfftp, bg, 4._dp*gcutw, ig_l2g, mill)
    deallocate(ig_l2g) 
    ngm = dfftp%ngl(mype+1)  
    npw = dffts%nwl(mype+1) 
    print '("npw values for rank",I8, "  = ",I8)', mype+1, npw 
    print '("ngm values for rank",I8, "  = ",I8)', mype+1, ngm
    !ngm_g = ngm 
    if (lpara) then 
      call mp_sum(ngm_g,comm) 
    end if   
  end subroutine initialize_miller_indices 
  ! 
  subroutine generate_wavefunctions() 
    implicit none
    real(dp),parameter  :: pi = 4._dp * atan(1._dp) 
    real(dp),allocatable  :: centers(:,:,:), sigmas(:,:)
    real(dp)              :: gg 
    complex(dp)           :: argstr
    integer               :: ig, iel, ice
    !
    allocate (wfcs(npw,nelec*ncenters)) 
    allocate(centers(3,nelec, ncenters), sigmas(nelec,ncenters)) 
    call random_number(centers) 
    call random_number(sigmas) 
    centers = 0.5_dp * centers
    sigmas = max(abs(sigmas), 0.2) 
    wfcs = cmplx(0._dp, 0._dp, kind=dp) 
    do ig = 1, npw 
       gg = dot_product(g(:,ig),g(:,ig)) 
       do iel = 1, nelec 
          do ice = 1, ncenters 
            argstr = cmplx(0._dp, 1._dp) * dot_product(g(:,ig),centers(1, iel, ice)*at(:,1)+&
                                                               centers(2, iel, ice)*at(:,2)+&
                                                               centers(3, iel, ice)*at(:,3))  
            wfcs(ig, (iel-1)*ncenters + ice ) = exp(-gg * sigmas(iel,ice)**2 / 4._dp ) * exp(argstr) 
          end do 
       end do 
    end do
  end subroutine generate_wavefunctions 
  ! 
  subroutine set_nl() 
    use fft_ggen, only: fft_set_nl 
    implicit none
    integer :: ig
    integer,allocatable :: issues(:)
    real(dp), parameter :: tpi = 8._dp * atan(1._dp)  
    g = g/tpi
    call fft_set_nl( dfftp, at, g)
    call fft_set_nl( dffts, at, g(:,1:npw)) 
    !check everything is fine with the grid we created 
    issues = pack([(ig, ig=1,dfftp%ngm)], dfftp%nl <= 0)
    if (size(issues) == 0) then    
            print '("sticks ok on ",I3)', mype
    else 
        print *, "Issues for sticks on rank ",mype, " for sticks ", issues
    end if  
  end subroutine set_nl 
  ! 
 
 subroutine compare_temp_with_starting(wfc1, wfc2, residual) 
   implicit none 
   complex(dp),intent(in)    :: wfc1(:), wfc2(:) 
   complex(dp),intent(inout) :: residual 
   integer                  :: ig 
   res = residual + sum(wfc1(1:npw) - wfc2(1:npw)) 
 end subroutine compare_temp_with_starting 
!
end program test_wfcs
!
