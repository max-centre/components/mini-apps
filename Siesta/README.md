# SIESTA MaX Mini-app
This is the SIESTA Mini-app from the MaX Centre of Excellence.

## Origin
_From which application/code the mini-apps comes from.
Original repo + associated release/branch/commit._


## Short description
In SIESTA, the lion's share of the computation time is spent in the
solver stage. One of the most relevant kernels is the diagonalization,
which in SIESTA is implemented fully by calls to external
libraries. In particular, the ELPA library is used for its
acceleration features.

ELPA is one of the code deliverables of the NOMAD CoE. A number of
miniapps related to the operation of the different ELPA kernels have
been prepared by BSC in that context.

Beyond diagonalization, SIESTA is able to use other
electronic-structure solvers. Most interesting is the PEXSI method,
which exhibits several levels of parallelization and a favorable
scaling.

There is a need to understand the balance of performance between the
different methods, depending on the system size and dimensionality,
and on the characteristics of the computer used. Work on a
meta-miniapp that drives both methods is scheduled to start during an
upcoming MaX hackathon.

## Build instructions
_How to build it.
List of dependencies._


## Run instructions
_How to execute it and get any relevant output as performance results, etc.
Please provide a methodology to validate the results._


## Test cases description
_Short description of the test case._

Ideally three test cases, if relevant:
_1. Single-node test case (Mandatory)
2. Multi-node test case
3. Big multi-node test case_


